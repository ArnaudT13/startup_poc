package fr.tse.projetpoc.controller;

import java.util.List;

import fr.tse.projetpoc.domain.Milestone;
import fr.tse.projetpoc.domain.Project;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import fr.tse.projetpoc.domain.Logs;
import fr.tse.projetpoc.exception.BadArgumentException;
import fr.tse.projetpoc.service.LogsService;
import fr.tse.projetpoc.service.PersonService;
import fr.tse.projetpoc.service.ProjectService;
import fr.tse.projetpoc.utils.Constants;
import javassist.tools.rmi.ObjectNotFoundException;

import javax.servlet.http.HttpServletResponse;

/**
 * Rest controller for Logs query
 */
@RestController
@CrossOrigin(origins = Constants.CROSS_ORIGIN, allowedHeaders = "*", maxAge = 3600, methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.OPTIONS, RequestMethod.PATCH, RequestMethod.DELETE}, allowCredentials = "True")
public class LogsController {
    @Autowired
    LogsService logsService;
    @Autowired
    ProjectService projectService;
    @Autowired
    PersonService personService;


    /**
     * Get all logs
     * @return List of logs
     */
    @ApiOperation("Find all logs.")
    @ApiResponses({
            @ApiResponse(code = HttpServletResponse.SC_OK, message = "Get all logs inside an array")
    })
    @GetMapping(value = "/logs",produces = "application/json")
    public List<Logs> findAllLogs(){
        return this.logsService.findAllLogs();
    }

    /**
     * Get one logs by id
     * @param id of logs
     * @return logs or null
     */
    @ApiOperation("Find logs by its id.")
    @ApiResponses({
            @ApiResponse(code = HttpServletResponse.SC_OK, message = "Get logs by id, null if it doesn't exist.")
    })
    @GetMapping(value = "/logs/{id}",produces = "application/json")
    public Logs findLogsById(@PathVariable Long id){
        return this.logsService.findLogsById(id);
    }

    /**
     * Get logs by project
     * @param id of project from which you want to get all logs
     * @return List of logs
     * @throws BadArgumentException The project doesn't exist, the project's id not found
     */
    @ApiOperation("Find project's logs.")
    @ApiResponses({
            @ApiResponse(code = HttpServletResponse.SC_OK, message = "Get logs from a project inside an array"),
            @ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, message = "The project doesn't exist, the project's id not found.")
    })
    @GetMapping(value = "/logs/projects/{id}",produces = "application/json")
    public List<Logs> findLogsByProject(@PathVariable Long id) throws BadArgumentException{
        if(this.projectService.findProjectById(id) == null)
            throw new BadArgumentException("The project ID doesn't exist");
        return this.logsService.findLogsByProject(this.projectService.findProjectById(id));
    }

    /**
     * Get logs from person
     * @param id fo person from which you will get all logs
     * @return List of logs
     * @throws BadArgumentException The person doesn't exist, the person's id not found
     */
    @ApiResponses({
            @ApiResponse(code = HttpServletResponse.SC_OK, message = "Get logs from a person inside an array"),
            @ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, message = "The person doesn't exist, the person's id not found.")
    })
    @ApiOperation("Find person's logs.")
    @GetMapping(value = "/logs/person/{id}",produces = "application/json")
    public List<Logs> findLogsByPerson(@PathVariable Long id) throws BadArgumentException{
        if(this.personService.findPersonById(id) == null)
            throw new BadArgumentException("The person ID doesn't exist");
        return this.logsService.findLogsByPerson(this.personService.findPersonById(id));
    }

    /**
     * Get logs from person and project
     * @param projectId id of project
     * @param personId id of person
     * @return List of logs
     * @throws BadArgumentException The project or the person doesn't exist, the project's id or person's id not found
     */
    @ApiOperation("Find person's logs from a project.")
    @ApiResponses({
            @ApiResponse(code = HttpServletResponse.SC_OK, message = "Get the person's logs from a project inside an array"),
            @ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, message = "The project or the person doesn't exist, the project's id or person's id not found.")
    })
    @GetMapping(value = "/logs/projects/{projectId}/person/{personId}",produces = "application/json")
    public List<Logs> findLogsByProjectAndPerson(@PathVariable(name = "projectId") Long projectId,@PathVariable(name = "personId") Long personId) throws BadArgumentException{
        if(this.personService.findPersonById(personId) == null)
            throw new BadArgumentException("The person ID doesn't exist");
        if(this.projectService.findProjectById(projectId) == null)
            throw new BadArgumentException("The project ID doesn't exist");
        return this.logsService.findLogsByPersonAndProject(this.personService.findPersonById(personId),this.projectService.findProjectById(projectId));
    }

    /**
     * Add logs
     * @param logs logs you want to add
     * @return Logs saved
     */
    @ApiOperation("Add logs.")
    @ApiResponses({
            @ApiResponse(code = HttpServletResponse.SC_OK, message = "The logs is added, return it.")
    })
    @PostMapping(value = "/logs/add",produces = "application/json")
    public ResponseEntity<Logs> addLogs(@RequestBody Logs logs){
        return new ResponseEntity<Logs>(this.logsService.addLogs(logs), HttpStatus.CREATED);
    }

    /**
     * Remove logs
     * @param id of logs to remove
     * @return ResponseEntity with HttpsStatus OK and message "Remove done"
     * @throws ObjectNotFoundException : The logs wasn't found, its id doesn't exist
     */
    @ApiOperation("Remove logs by id.")
    @ApiResponses({
            @ApiResponse(code = HttpServletResponse.SC_OK, message = "Remove the logs. Return a confirmation message"),
            @ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, message = "The logs wasn't found, its id doesn't exist.")
    })
    @DeleteMapping (value = "/logs/remove/{id}", produces = "application/json")
    public ResponseEntity<String> removeLogs(@PathVariable Long id) throws ObjectNotFoundException {
        this.logsService.removeLogs(this.logsService.findLogsById(id));
        return new ResponseEntity<String>("Remove done", HttpStatus.OK);
    }

    /**
     * Find the milestone of the logs
     * @param id : the logs param
     * @return Milestone
     * @throws BadArgumentException : The logs doesn't exist, the logs' id was not found
     */
    @ApiOperation("Get the logs' milestone.")
    @GetMapping(value="/logs/{id}/milestone")
    @ApiResponses({
            @ApiResponse(code = HttpServletResponse.SC_OK, message = "Get the milestone of logs inside an array"),
            @ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, message = "The logs doesn't exist, the logs' id was not found.")
    })
    public ResponseEntity<?> findMilestoneFromLogs(@PathVariable Long id)throws BadArgumentException{
        return new ResponseEntity<Milestone>(this.logsService.getMilestoneFromLogs(id),HttpStatus.OK);
    }

    /**
     * Find the project of the logs
     * @param id : the logs param
     * @return Project
     * @throws BadArgumentException : The logs doesn't exist, the logs' id was not found.
     */
    @ApiOperation("Get the logs' project.")
    @ApiResponses({
            @ApiResponse(code = HttpServletResponse.SC_OK, message = "Get the project from the logs"),
            @ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, message = "The logs doesn't exist, the logs' id was not found.")
    })
    @GetMapping(value="/logs/{id}/project")
    public ResponseEntity<?> findProjectFromLogs(@PathVariable Long id)throws BadArgumentException{
        return new ResponseEntity<Project>(this.logsService.getProjectFromLogs(id),HttpStatus.OK);
    }

    /**
     * Modify a logs
     * @param id : the id of the logs to modify
     * @param logs : the new logs (will change the description, the edit date and the granularity)
     * @return
     * @throws BadArgumentException : The logs doesn't exist, the logs' id was not found.
     */
    @ApiOperation("Modify logs.")
    @ApiResponses({
            @ApiResponse(code = HttpServletResponse.SC_OK, message = "Logs modified, return it."),
            @ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, message = "The logs doesn't exist, the logs' id was not found.")
    })
    @PatchMapping(value ="/logs/{id}/modif/")
    public ResponseEntity<?> modLogs(@PathVariable(name = "id") Long id,@RequestBody Logs logs)throws BadArgumentException{
        return new ResponseEntity<Logs>(this.logsService.modLogs(id,logs.getEditDate(),logs.getGranularity(),logs.getDescription()),HttpStatus.OK);
    }
}
