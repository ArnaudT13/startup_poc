package fr.tse.projetpoc.controller;

import java.util.List;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.reflections.Reflections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import fr.tse.projetpoc.domain.Logs;
import fr.tse.projetpoc.domain.Milestone;
import fr.tse.projetpoc.domain.Project;
import fr.tse.projetpoc.exception.BadArgumentException;
import fr.tse.projetpoc.service.MilestoneService;
import fr.tse.projetpoc.service.PersonService;
import fr.tse.projetpoc.service.ProjectService;
import fr.tse.projetpoc.utils.Constants;
import javassist.tools.rmi.ObjectNotFoundException;

import javax.servlet.http.HttpServletResponse;

/**
 * Rest controller for Milestone query
 */
@RestController
@CrossOrigin(origins = Constants.CROSS_ORIGIN, allowedHeaders = "*", maxAge = 3600, methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.OPTIONS, RequestMethod.PATCH,RequestMethod.DELETE}, allowCredentials = "True")
public class
MilestoneController {
    @Autowired
    MilestoneService milestoneService;
    @Autowired
    ProjectService projectService;
    @Autowired
    PersonService personService;

    /**
     * Use to get all milestones
     * @return list of milestones
     */
    @ApiOperation("Find all milestones.")
    @ApiResponses({
            @ApiResponse(code = HttpServletResponse.SC_OK, message = "Get the milestones")
    })
    @GetMapping(value = "/milestones",produces = "application/json")
    public List<Milestone> findAllMilestones(){
        return this.milestoneService.findAllMilestones();
    }

    /**
     * Get milestone from id
     * @param id of milestone
     * @return Milestone or null
     */
    @ApiOperation("Find a milestone from its id.")
    @ApiResponses({
            @ApiResponse(code = HttpServletResponse.SC_OK, message = "Get the milestone by its id, null if not found.")
    })
    @GetMapping(value = "/milestones/{id}",produces = "application/json")
    public Milestone findMilestoneById(@PathVariable Long id){
        return this.milestoneService.findMilestoneById(id);
    }

    /**
     * Get milestones from project
     * @param projectId : the id of the project
     * @return list of milestones
     */
    @ApiOperation("Find project's milestones.")
    @ApiResponses({
            @ApiResponse(code = HttpServletResponse.SC_OK, message = "Get the milestone of a project inside an array"),
            @ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, message = "The project doesn't exist, the project's id was not found.")
    })
    @GetMapping(value = "/milestones/projects/{id}",produces = "application/json")
    public List<Milestone> findMilestoneByProject(@PathVariable(value = "id") Long projectId) throws BadArgumentException{
        Project project = this.projectService.findProjectById(projectId);
        if(project != null){
            return this.milestoneService.findMilestonesByProject(project);
        }else{
            throw new BadArgumentException("The milestone doesn't exist");
        }
    }

    /**
     * Get Project of milestone
     * @param id : the id of the milestone
     * @return the project
     * @throws BadArgumentException The milestone doesn't exist, the milestone's id was not found
     */
    @ApiOperation("Find milestone's project.")
    @ApiResponses({
            @ApiResponse(code = HttpServletResponse.SC_OK, message = "Get the project of the milestone."),
            @ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, message = "The milestone doesn't exist, the milestone's id was not found.")
    })
    @GetMapping(value="/milestones/{id}/project",produces = "application/json")
    public Project findProjectFromMilestone(@PathVariable Long id) throws BadArgumentException {
        return this.milestoneService.findProjectFromMilestone(id);
    }

    /**
     * Add milestone
     * @param milestone : the milestone to add
     * @return milestone saved
     */
    @ApiOperation("Add a milestone.")
    @ApiResponses({
            @ApiResponse(code = HttpServletResponse.SC_OK, message = "Milestone added, return it.")
    })
    @PostMapping(value = "project/{id}/milestones/add",produces = "application/json")
    public ResponseEntity<Milestone> addMilestone(@PathVariable Long id,@RequestBody Milestone milestone) throws BadArgumentException {
        return new ResponseEntity<Milestone>(this.milestoneService.addMilestone(id, milestone), HttpStatus.CREATED);
    }

    /**
     * Rome milestone
     * @param id of the milestone
     * @return Https responseEntity with HttpsStatus OK and message "Remove done"
     * @throws ObjectNotFoundException The milestone doesn't exist, the milestone's id was not found
     */
    @ApiOperation("Remove a milestone.")
    @ApiResponses({
            @ApiResponse(code = HttpServletResponse.SC_OK, message = "Remove the milestone, return a success's message"),
            @ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, message = "The milestone doesn't exist, the milestone's id was not found.")
    })
    @DeleteMapping(value = "/milestones/remove/{id}", produces = "application/json")
    public ResponseEntity<String> removeMilestone(@PathVariable Long id) throws ObjectNotFoundException{
        this.milestoneService.removeMilestone(this.milestoneService.findMilestoneById(id));
        return new ResponseEntity<String>("Remove done",HttpStatus.OK);
    }

    /**
     * Add logs into milestone with person who worked on the logs
     * @param milestoneId the id of the milestone in which logs will be added
     * @param personId the id of the person
     * @param logs the logs you want to add
     * @return the milestone modified
     * @throws ObjectNotFoundException The milestone or the person doesn't exist, the milestone's id or the person's id were not found
     */
    @ApiOperation("Add a logs with the person in charge in a milestone.")
    @ApiResponses({
            @ApiResponse(code = HttpServletResponse.SC_OK, message = "Return the milestone where the logs was added."),
            @ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, message = "The milestone or the person doesn't exist, the milestone's id or the person's id were not found.")
    })
    @PatchMapping(value = "/milestones/{milestoneId}/persons/{personId}/logs/add",produces = "application/json")
    public ResponseEntity<Milestone> addLogsInMilestone(@PathVariable(name = "milestoneId")Long milestoneId,
                                                        @PathVariable(name="personId")Long personId,
                                                        @RequestBody Logs logs ) throws BadArgumentException,ObjectNotFoundException{
        return new ResponseEntity<>(this.milestoneService.addLogsInMilestone(milestoneId,logs,this.personService.findPersonById(personId)),HttpStatus.OK);
    }


}
