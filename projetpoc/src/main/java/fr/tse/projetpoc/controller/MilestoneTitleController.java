package fr.tse.projetpoc.controller;

import java.util.List;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import fr.tse.projetpoc.domain.MilestoneTitle;
import fr.tse.projetpoc.service.MilestoneTitleService;
import fr.tse.projetpoc.utils.Constants;

import javax.servlet.http.HttpServletResponse;

/**
 * Rest controller for MilestoneTitle query
 */
@RestController
@CrossOrigin(origins = Constants.CROSS_ORIGIN, allowedHeaders = "*", maxAge = 3600, methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.OPTIONS, RequestMethod.PATCH}, allowCredentials = "True")
public class MilestoneTitleController {
    @Autowired
    MilestoneTitleService milestoneTitleService;

    /**
     * Get all milestones' titles from database
     * @return all milestones' titles
     */
    @ApiOperation("Find all milestones' titles.")
    @ApiResponses({
            @ApiResponse(code = HttpServletResponse.SC_OK, message = "Return all milestones' title inside an array.")
    })
    @GetMapping(value = "/milestones/titles",produces = "application/json")
    public List<MilestoneTitle> findAllMilestonesTitles(){
        return this.milestoneTitleService.findAllMilestonesTitle();
    }

    /**
     * Get milestone's title with id
     * @param id of milestone's title
     * @return milestone's title
     */
    @ApiOperation("Find milestone's title by its id.")
    @ApiResponses({
            @ApiResponse(code = HttpServletResponse.SC_OK, message = "Return the milestone's title by its id, null if not found.")
    })
    @GetMapping(value = "/milestones/titles/{id}",produces = "application/json")
    public MilestoneTitle findMilestoneTitleById(@PathVariable Long id){
        return this.milestoneTitleService.findMilestonesTitleById(id);
    }

    /**
     * Add milestone's title
     * @param milestoneTitle the object representing MilestoneTitle
     * @return MilestoneTitle saved
     */
    @ApiOperation("Add a milestone's title.")
    @ApiResponses({
            @ApiResponse(code = HttpServletResponse.SC_OK, message = "Milestone's title added, return it.")
    })
    @PostMapping(value = "/milestones/titles/add",produces = "application/json")
    public ResponseEntity<MilestoneTitle> addMilestoneTitle(@RequestBody MilestoneTitle milestoneTitle){
        return new ResponseEntity<MilestoneTitle>(this.milestoneTitleService.addMilestoneTitle(milestoneTitle),HttpStatus.CREATED);
    }
}
