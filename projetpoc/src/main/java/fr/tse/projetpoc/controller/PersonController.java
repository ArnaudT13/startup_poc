package fr.tse.projetpoc.controller;

import java.security.Principal;
import java.util.List;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import fr.tse.projetpoc.domain.Person;
import fr.tse.projetpoc.domain.Project;
import fr.tse.projetpoc.domain.Role;
import fr.tse.projetpoc.exception.NotManagerException;
import fr.tse.projetpoc.exception.UserLoginAlreadyExists;
import fr.tse.projetpoc.service.PersonService;
import fr.tse.projetpoc.service.RoleService;
import fr.tse.projetpoc.utils.Constants;
import javassist.tools.rmi.ObjectNotFoundException;

import javax.servlet.http.HttpServletResponse;

/**
 * Rest controller for Person query
 */
@RestController
@Slf4j
@CrossOrigin(origins = Constants.CROSS_ORIGIN, allowedHeaders = "*", maxAge = 3600, methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.OPTIONS, RequestMethod.PATCH}, allowCredentials = "True")
public class PersonController {
    @Autowired
    private PersonService personService;
    @Autowired
    private RoleService roleService;
    @Autowired
    PasswordEncoder passwordEncoder;


    /**
     * Use to got all people
     *
     * @return JSON list of persons
     */
    @ApiOperation("Find all persons.")
    @ApiResponses({
            @ApiResponse(code = HttpServletResponse.SC_OK, message = "Return all persons inside an array.")
    })
    @GetMapping("/persons")
    public ResponseEntity<?> findAllPersons() {
        return new ResponseEntity<List<Person>>(this.personService.findAllPerson(), HttpStatus.OK);
    }

    /**
     * Get all people with the same firstName
     *
     * @param firstName : the firstName for filtering
     * @return JSON list of people with same firstName
     */
    @ApiOperation("Find persons by the firstname.")
    @ApiResponses({
            @ApiResponse(code = HttpServletResponse.SC_OK, message = "Return all the persons who have the same firstname inside an array.")
    })
    @GetMapping("/persons/firstName")
    public ResponseEntity<?> findPersonByFirstName(@RequestParam(name = "firstName") String firstName) {
        return new ResponseEntity<List<Person>>(this.personService.findPersonsByFirstName(firstName), HttpStatus.OK);
    }

    /**
     * Get all people with the same lastName
     *
     * @param lastName : the lastName for filtering
     * @return JSON list of people with same lastName
     */
    @ApiOperation("Find persons by the lastname.")
    @ApiResponses({
            @ApiResponse(code = HttpServletResponse.SC_OK, message = "Return all the persons who have the same lastname inside an array.")
    })
    @GetMapping("/persons/lastName")
    public ResponseEntity<?> findPersonByLastName(@RequestParam(name = "lastName") String lastName) {
        return new ResponseEntity<List<Person>>(this.personService.findPersonsByLastName(lastName), HttpStatus.OK);
    }
    
    /**
     * Get all people with the same username
     *
     * @param username : the lastName for filtering
     * @return JSON list of people with same username
     */
    @ApiOperation("Find persons by the username.")
    @ApiResponses({
            @ApiResponse(code = HttpServletResponse.SC_OK, message = "Return the person who has the username, null if nobody has it.")
    })
    @GetMapping("/person/username")
    public ResponseEntity<?> findPersonByUsername(@RequestParam(name = "username") String username) {
        return new ResponseEntity<Person>(this.personService.findPersonsByUsername(username).get(0), HttpStatus.OK);
    }

    /**
     * One person with the id
     *
     * @param id : the id for filtering
     * @return null or JSON of person
     */
    @ApiOperation("Find person by his id.")
    @ApiResponses({
            @ApiResponse(code = HttpServletResponse.SC_OK, message = "Return the person by its id, null if not found.")
    })
    @GetMapping("/person/{id}")
    public ResponseEntity<?> findPersonById(@PathVariable Long id) {
        return new ResponseEntity<Person>(this.personService.findPersonById(id), HttpStatus.OK);
    }

    /**
     * Add person to database
     *
     * @param person : the person to add
     * @return person saved
     * @throws UserLoginAlreadyExists The user login already exists in database
     */
    @PreAuthorize("hasAuthority('Admin')")
    @ApiOperation("Add a person.")
    @ApiResponses({
            @ApiResponse(code = HttpServletResponse.SC_OK, message = "Person added, return it."),
            @ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, message = "The username is already used by someone."),
            @ApiResponse(code = HttpServletResponse.SC_FORBIDDEN, message = "The request must be from an admin user logged.")
    })
    @PostMapping(value = "/person/add", produces = "application/json")
    public ResponseEntity<?> addPerson(@RequestBody Person person) throws UserLoginAlreadyExists {
        person.setPassword(this.passwordEncoder.encode(person.getPassword()));
        Person personResponse = this.personService.addPerson(person);
        return new ResponseEntity<Person>(personResponse, HttpStatus.CREATED);
    }

    /**
     * Remove person from database
     *
     * @param id : the person's id to remove
     * @return A sucess message
     * @throws ObjectNotFoundException
     */
    @PreAuthorize("hasAuthority('Admin')")
    @ApiOperation("Remove a person.")
    @ApiResponses({
            @ApiResponse(code = HttpServletResponse.SC_OK, message = "Person removed, success's message returned."),
            @ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, message = "The person doesn't exist, person's id not found ."),
            @ApiResponse(code = HttpServletResponse.SC_FORBIDDEN, message = "The request must be from an admin user logged.")
    })
    @PatchMapping(value = "person/{id}/remove", produces = "application/json")
    public ResponseEntity<?> removePersone(@PathVariable Long id) throws ObjectNotFoundException {
        this.personService.deletePerson(id);
        return new ResponseEntity<>("", HttpStatus.OK);
    }

    /**
     * Modify person role
     *
     * @param idP person's id
     * @param idR role's id
     * @return the person modified
     * @throws ObjectNotFoundException
     */
    @PreAuthorize("hasAuthority('Admin')")
    @ApiOperation("Change the person's role.")
    @ApiResponses({
            @ApiResponse(code = HttpServletResponse.SC_OK, message = "Person's role changed, return the person."),
            @ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, message = "The person doesn't exist, person's id not found."),
            @ApiResponse(code = HttpServletResponse.SC_FORBIDDEN, message = "The request must be from an admin user logged.")
    })
    @PatchMapping(value = "person/{idP}/role/{idR}", produces = "application/json")
    public ResponseEntity<?> modifyPersonRole(@PathVariable Long idP, @PathVariable Long idR) throws ObjectNotFoundException {
        Person person = this.personService.modifyRole(this.personService.findPersonById(idP), this.roleService.findRoleById(idR));
        return new ResponseEntity<>(person, HttpStatus.OK);
    }

    /**
     * Use to retrieve all the persons related to a manager
     *
     * @return JSON list of persons
     * @throws NotManagerException     The manager has no manager role
     * @throws ObjectNotFoundException The manager doesn't exist in database
     */
    @ApiOperation("Find manager's developers.")
    @ApiResponses({
            @ApiResponse(code = HttpServletResponse.SC_OK, message = "Return the developers below a manager inside an array."),
            @ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, message = "The manager doesn't exist , manager's id not found ."),
            @ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, message = "The person isn't a manager.")
    })
    @GetMapping("/persons/{id}/developers")
    public ResponseEntity<?> findAllPersonsForAManager(@PathVariable Long id) throws ObjectNotFoundException, NotManagerException {
        return new ResponseEntity<List<Person>>(this.personService.findAllDeveloperForManager(this.personService.findPersonById(id)), HttpStatus.OK);
    }


    /**
     * Use to retrieve all the projects related to a person
     *
     * @return JSON list of projects
     * @throws ObjectNotFoundException The person doesn't exist in database
     */
    @ApiOperation("Find the person's projects.")
    @ApiResponses({
            @ApiResponse(code = HttpServletResponse.SC_OK, message = "Return the projects of the person inside an array."),
            @ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, message = "The person doesn't exist , person's id not found .")
    })
    @GetMapping("/persons/{id}/projects")
    public ResponseEntity<?> findAllProjectForPerson(@PathVariable Long id) throws ObjectNotFoundException {
        return new ResponseEntity<List<Project>>(this.personService.findAllProjectForPerson(this.personService.findPersonById(id)), HttpStatus.OK);
    }

    /**
     * Use to change the user's manager
     * @return JSON response to validate the changement of manager for user
     * @throws ObjectNotFoundException The user/manager doesn't exist
     * @throws NotManagerException The manager requested doesn't have the good role
     */
    @PreAuthorize("hasAuthority('Manager') || hasAuthority('Admin')")
    @ApiOperation("Change the person's manager.")
    @ApiResponses({
            @ApiResponse(code = HttpServletResponse.SC_OK, message = "The person's manager has been changed."),
            @ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, message = "The person or the manager doesn't exist , person's id or manager's id not found.\nThe person informed as manager isn't a manager."),
            @ApiResponse(code = HttpServletResponse.SC_FORBIDDEN, message = "The request must be from a manager or an admin user logged.")
    })
    @PatchMapping(value = "person/{idP}/manager/{idM}", produces = "application/json")
    public ResponseEntity<?> changeUserManager(@PathVariable(value = "idP") Long idP,@PathVariable(value = "idM") Long idM) throws ObjectNotFoundException,NotManagerException {
        this.personService.addOrChangeManager(this.personService.findPersonById(idP), this.personService.findPersonById(idM));
        return new ResponseEntity<>(HttpStatus.OK);
    }

    /**
     * Remove a manager for a person
     * @return JSON response to remove the manager
     * @throws ObjectNotFoundException The manager doesn't exist
     */
    @PreAuthorize("hasAuthority('Manager') || hasAuthority('Admin')")
    @ApiOperation("Remove the person's manager.")
    @ApiResponses({
            @ApiResponse(code = HttpServletResponse.SC_OK, message = "The person's manager has been removed."),
            @ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, message = "The person doesn't exist , person's id not found ."),
            @ApiResponse(code = HttpServletResponse.SC_FORBIDDEN, message = "The request must be from a manager or an admin user logged.")
    })
    @PatchMapping(value = "/person/{idP}/remove_manager", produces = "application/json")
    public ResponseEntity<?> removeManager(@PathVariable(value = "idP") Long idP) throws ObjectNotFoundException{
        this.personService.deleteManager(this.personService.findPersonById(idP));
        return new ResponseEntity<>(HttpStatus.OK);
    }
    
}

