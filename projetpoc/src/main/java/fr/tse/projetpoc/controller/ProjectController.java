package fr.tse.projetpoc.controller;

import java.util.List;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import fr.tse.projetpoc.dao.MilestoneRepository;
import fr.tse.projetpoc.dao.PersonRepository;
import fr.tse.projetpoc.domain.Milestone;
import fr.tse.projetpoc.domain.Person;
import fr.tse.projetpoc.domain.Project;
import fr.tse.projetpoc.exception.BadArgumentException;
import fr.tse.projetpoc.service.ProjectService;
import fr.tse.projetpoc.utils.Constants;
import javassist.tools.rmi.ObjectNotFoundException;

import javax.servlet.http.HttpServletResponse;

/**
 * Rest controller for Project query
 */
@RestController
@CrossOrigin(origins = Constants.CROSS_ORIGIN, allowedHeaders = "*", maxAge = 3600, methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.OPTIONS, RequestMethod.PATCH, RequestMethod.DELETE}, allowCredentials = "True")
public class ProjectController {
    @Autowired
    private ProjectService projectService;
    @Autowired
    private PersonRepository personRepository;
    @Autowired
    private MilestoneRepository milestoneRepository;

    /**
     * Get all projects
     * @return All projects from database
     */
    @ApiOperation("Find all projects.")
    @ApiResponses({
            @ApiResponse(code = HttpServletResponse.SC_OK, message = "Get all projects inside an array.")
    })
    @GetMapping("/projects")
    public ResponseEntity<?> findAllProjects(){
        return new ResponseEntity<List<Project>>(this.projectService.findAllProject(), HttpStatus.OK);
    }

    /**
     * Get all projects with the same name
     * @param name : the same for filtering
     * @return all projects with same name
     */
    @ApiOperation("Find projects by its name.")
    @ApiResponses({
            @ApiResponse(code = HttpServletResponse.SC_OK, message = "Get all projects with the same name inside an array.")
    })
    @GetMapping("/projects/name")
    public ResponseEntity<?> findProjectsByName(@RequestParam(name = "name") String name){
        return new ResponseEntity<List<Project>>(this.projectService.findProjectsByName(name), HttpStatus.OK);
    }

    /**
     * Get project by id
     * @param id : the id of project to get
     * @return null or JSON of project
     */
    @ApiOperation("Find project by id.")
    @ApiResponses({
            @ApiResponse(code = HttpServletResponse.SC_OK, message = "Return project from its id, null if not found.")
    })
    @GetMapping("/projects/{id}")
    public ResponseEntity<?> findProjectById(@PathVariable Long id){
        return new ResponseEntity<Project>(this.projectService.findProjectById(id), HttpStatus.OK);
    }

    /**
     * Get contributors of project
     * @param id : the id of project for the contributor
     * @return null or JSON of person array
     */
    @ApiOperation("Find contributors of project by id's project.")
    @ApiResponses({
            @ApiResponse(code = HttpServletResponse.SC_OK, message = "Return contributors from the project id")
    })
    @GetMapping("/projects/{id}/persons")
    public ResponseEntity<?> getContriutorsByProject(@PathVariable Long id) throws BadArgumentException {
        return new ResponseEntity<List<Person>>(this.projectService.getContributor(id), HttpStatus.OK);
    }


    /**
     * Add project to database
     * @param project : the project to add in database
     * @return project saved
     */
    @PreAuthorize("hasAuthority('Admin') || hasAuthority('Manager')")
    @ApiOperation("Add a project.")
    @ApiResponses({
            @ApiResponse(code = HttpServletResponse.SC_OK, message = "Project created, return it."),
            @ApiResponse(code = HttpServletResponse.SC_FORBIDDEN, message = "The request must be from a manager or an admin user logged.")
    })
    @PostMapping(value = "project/add",produces = "application/json")
    public ResponseEntity<?> addProject(@RequestBody Project project){
        return new ResponseEntity<Project>(this.projectService.addProject(project),HttpStatus.CREATED);
    }

    /**
     * Remove project from database
     * @param project : the project to remove
     * @return sucess message
     * @throws ObjectNotFoundException
     */
    @PreAuthorize("hasAuthority('Admin') || hasAuthority('Manager')")
    @ApiOperation("Remove a project.")
    @ApiResponses({
            @ApiResponse(code = HttpServletResponse.SC_OK, message = "Project removed, success's message returned."),
            @ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, message = "Project doesn't exist, the project's id was not found."),
            @ApiResponse(code = HttpServletResponse.SC_FORBIDDEN, message = "The request must be from a manager or an admin user logged.")
    })
    @DeleteMapping(value = "project/remove",produces = "application/json")
    public ResponseEntity<?> removeProject(@RequestBody Project project) throws ObjectNotFoundException {
        this.projectService.deleteProject(project);
        return new ResponseEntity<>("Remove done",HttpStatus.OK);
    }

    /**
     * Add milestone to project
     * @param id id of project
     * @param milestone milestone to add
     * @return Project modified
     */
    @PreAuthorize("hasAuthority('Admin') || hasAuthority('Manager')")
    @ApiOperation("Add a milestone to a project.")
    @ApiResponses({
            @ApiResponse(code = HttpServletResponse.SC_OK, message = "Milestone added to the project, project returned."),
            @ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, message = "Project doesn't exist, the project's id was not found."),
            @ApiResponse(code = HttpServletResponse.SC_FORBIDDEN, message = "The request must be from a manager or an admin user logged.")
    })
    @PatchMapping(value = "/projects/{id}/add_milestone", produces = "application/json")
    public ResponseEntity<?> addMilestone(@PathVariable Long id,@RequestBody Milestone milestone) throws BadArgumentException{
       return new ResponseEntity<Project>(this.projectService.addMilestoneInProject(id,milestone),HttpStatus.OK);
    }

    /**
     * Remove milestone from project
     * @param projectId : id of project
     * @param milestoneId : id of milestone
     * @return Project modified
     * @throws ObjectNotFoundException 
     */
    @PreAuthorize("hasAuthority('Admin') || hasAuthority('Manager')")
    @ApiOperation("Remove a milestone from a project.")
    @ApiResponses({
            @ApiResponse(code = HttpServletResponse.SC_OK, message = "Milestone removed from the project, project returned."),
            @ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, message = "Project or milestone don't exist, the project's id or milestone's id were not found." +
                    "Milestone isn't a milestone the project."),
            @ApiResponse(code = HttpServletResponse.SC_FORBIDDEN, message = "The request must be from a manager or an admin user logged.")
    })
    @PatchMapping(value = "/projects/{id_project}/delete_milestone/{id_milestione}", produces = "application/json")
    public ResponseEntity<?> deleteMilestone(@PathVariable(name = "id_project") Long projectId,@PathVariable(name = "id_milestione") Long milestoneId) throws BadArgumentException, ObjectNotFoundException{
        Milestone milestone = this.milestoneRepository.findById(milestoneId)
                .orElseThrow(() -> new BadArgumentException("Wrong id argument"));

        return new ResponseEntity<Project>(this.projectService.deleteMilestoneInProject(projectId,milestone),HttpStatus.OK);
    }

    /**
     * Add contributor to project
     * @param id : id of project
     * @param id_contributor : the if of the contributor to add (Person.class)
     * @return Project modified
     */
    @PreAuthorize("hasAuthority('Admin') || hasAuthority('Manager')")
    @ApiOperation("Add a contributor to a project.")
    @ApiResponses({
            @ApiResponse(code = HttpServletResponse.SC_OK, message = "Developer added to the project, return the project"),
            @ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, message = "Project doesn't exist, the project's id was not found."),
            @ApiResponse(code = HttpServletResponse.SC_FORBIDDEN, message = "The request must be from a manager or an admin user logged.")
    })
    @PatchMapping(value = "/projects/{id}/add_contributor/{id_contributor}", produces = "application/json")
    public ResponseEntity<?> addContributor(@PathVariable Long id,@PathVariable(value = "id_contributor") Long id_contributor) throws BadArgumentException {
        Person contributor = this.personRepository.findById(id_contributor).orElseThrow(() -> new BadArgumentException("Id of contributor not found."));
        return new ResponseEntity<Project>(this.projectService.addContributorInProject(id,contributor),HttpStatus.OK);
    }

    /**
     * Remove contributor from project
     * @param projectId : id of project
     * @param contributorId : id of contributor
     * @return Project modified
     */
    @PreAuthorize("hasAuthority('Admin') || hasAuthority('Manager')")
    @ApiOperation("Remove a contributor from a project.")
    @ApiResponses({
            @ApiResponse(code = HttpServletResponse.SC_OK, message = "Developer removed from the project, return the project."),
            @ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, message = "Project or the developer/manager don't exist, the project's or the contributor's id were not found. " +
                    "The person isn't a contributor of the project."),
            @ApiResponse(code = HttpServletResponse.SC_FORBIDDEN, message = "The request must be from a manager or an admin user logged.")
    })
    @PatchMapping(value = "/projects/{id_project}/delete_contributor/{id_contributor}", produces = "application/json")
    public ResponseEntity<?> deleteContributor(@PathVariable(name = "id_project") Long projectId,@PathVariable(name = "id_contributor") Long contributorId) throws BadArgumentException{
        Person contributor = this.personRepository.findById(contributorId)
                .orElseThrow(() -> new BadArgumentException("Wrong id argument"));
        return new ResponseEntity<Project>(this.projectService.deleteContributorInProject(projectId,contributor),HttpStatus.OK);
    }
}
