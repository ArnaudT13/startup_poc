package fr.tse.projetpoc.controller;

import fr.tse.projetpoc.domain.Person;
import fr.tse.projetpoc.service.PersonService;
import fr.tse.projetpoc.service.ReportService;
import fr.tse.projetpoc.utils.Constants;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperPrint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.time.LocalDate;

/**
 * Rest controller for Report query
 */
@RestController
@CrossOrigin(origins = Constants.CROSS_ORIGIN, allowedHeaders = "*", maxAge = 3600, methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.OPTIONS, RequestMethod.PATCH}, allowCredentials = "True")
public class ReportController {
    @Autowired
    ReportService reportService;
    @Autowired
    PersonService personService;

    /**
     * Get the pdf report of a person between 2 dates.
     * @param id : the person's id
     * @param startDate : the date from which the report will start
     * @param endDate : the end date of the report
     * @return a pdf report into a reponse entity with its name in the content_disposition
     * @throws Exception : throw if there is an error in the generation of the report (no user, intern problem)
     */
    @ApiOperation("Get the person's report between two dates.")
    @ApiResponses({
            @ApiResponse(code = HttpServletResponse.SC_OK, message = "Return the person's report into a pdf format with its name in the content_disposition."),
            @ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, message = "The person doesn't exist, the person's id was not found." +
                    "There was an error while creating the report."),
            @ApiResponse(code = HttpServletResponse.SC_FORBIDDEN, message = "The request must be from a manager or an admin user logged.")
    })
    @GetMapping(value = "/persons/{id}/report/{dateStart}/{dateEnd}",produces = "application/pdf")
    public ResponseEntity<byte[]> getReport(@PathVariable(name = "id")Long id, @PathVariable(name = "dateStart")String startDate, @PathVariable(name = "dateEnd")String endDate) throws Exception {
        JasperPrint jp = this.reportService.getReport(id,LocalDate.parse(startDate),LocalDate.parse(endDate));
        if(jp == null){
            throw new Exception("Jasper print is null");
        }
        Person person = this.personService.findPersonById(id);
        String pdfName = person.getFirstName()+"_"+person.getLastName()+"_"+startDate.toString()+"_"+endDate.toString()+".pdf";
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.CONTENT_DISPOSITION,"filename="+pdfName);
        return new ResponseEntity<byte[]>(JasperExportManager.exportReportToPdf(jp),headers,HttpStatus.OK);
    }
}
