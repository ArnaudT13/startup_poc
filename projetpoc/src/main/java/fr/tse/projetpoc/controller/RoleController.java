package fr.tse.projetpoc.controller;

import java.util.List;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import fr.tse.projetpoc.domain.Role;
import fr.tse.projetpoc.service.RoleService;
import fr.tse.projetpoc.utils.Constants;

import javax.servlet.http.HttpServletResponse;

/**
 * Rest controller for Role query
 */
@RestController
@CrossOrigin(origins = Constants.CROSS_ORIGIN, allowedHeaders = "*", maxAge = 3600, methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.OPTIONS, RequestMethod.PATCH}, allowCredentials = "True")
public class RoleController{
    @Autowired
    RoleService roleService;

    /**
     * Get all roles from database
     * @return all roles
     */
    @ApiOperation("Find all the roles.")
    @ApiResponses({
            @ApiResponse(code = HttpServletResponse.SC_OK, message = "Return all roles inside an array.")
    })
    @GetMapping(value = "/roles",produces = "application/json")
    public List<Role> findAllRoles(){
        return this.roleService.findAllRole();
    }

    /**
     * Get role by id
     * @param id : id for filtering
     * @return null or 1 role
     */
    @ApiOperation("Find role by id.")
    @ApiResponses({
            @ApiResponse(code = HttpServletResponse.SC_OK, message = "Return role by its id, null if id not found.")
    })
    @GetMapping(value = "/roles/{id}",produces = "application/json")
    public Role findRoleById(@PathVariable Long id){
        return this.roleService.findRoleById(id);
    }


}
