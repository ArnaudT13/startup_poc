package fr.tse.projetpoc.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.tse.projetpoc.domain.Logs;
import fr.tse.projetpoc.domain.Milestone;
import fr.tse.projetpoc.domain.Person;

/**
 * The logs JpaRepository interface
 */
@Repository
public interface LogsRepository extends JpaRepository<Logs, Long> {
    public List<Logs> findByPerson(Person person);
    public List<Logs> findByMilestone(Milestone milestone);
}
