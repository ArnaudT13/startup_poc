package fr.tse.projetpoc.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.tse.projetpoc.domain.Milestone;
import fr.tse.projetpoc.domain.Project;

/**
 * The milestone JpaRepository interface
 */
@Repository
public interface MilestoneRepository extends JpaRepository<Milestone, Long>{
    public List<Milestone> findMilestonesByProject(Project project);
    public List<Milestone> findMilestonesByDescription(String description);
}
