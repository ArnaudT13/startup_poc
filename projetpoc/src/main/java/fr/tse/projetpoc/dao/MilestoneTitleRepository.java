package fr.tse.projetpoc.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.tse.projetpoc.domain.MilestoneTitle;

/**
 * The milestone title JpaRepository interface
 */
@Repository
public interface MilestoneTitleRepository extends JpaRepository<MilestoneTitle, Long>{

}
