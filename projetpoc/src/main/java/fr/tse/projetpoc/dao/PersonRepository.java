package fr.tse.projetpoc.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.tse.projetpoc.domain.Person;

/**
 * The person JpaRepository interface
 */
@Repository
public interface PersonRepository extends JpaRepository<Person, Long>{
    public List<Person> findByFirstName(String firstName);
    public List<Person> findByLastName(String lastName);
    public List<Person> findByLogin(String login);
}
