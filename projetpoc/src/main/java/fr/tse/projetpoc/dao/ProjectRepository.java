package fr.tse.projetpoc.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.tse.projetpoc.domain.Project;

/**
 * The project JpaRepository interface
 */
@Repository
public interface ProjectRepository extends JpaRepository<Project, Long> {
    public List<Project> findByName(String name);
}
