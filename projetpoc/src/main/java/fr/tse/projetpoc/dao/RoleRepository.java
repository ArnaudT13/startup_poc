package fr.tse.projetpoc.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.tse.projetpoc.domain.Role;

/**
 * The role Jparepository interface
 */
@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {

}
