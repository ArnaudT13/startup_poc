package fr.tse.projetpoc.domain;


import java.time.LocalDate;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.NotFound;
import org.springframework.beans.factory.annotation.Value;

/**
 * Class which represents logs in the project
 */
@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class Logs {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Min(value = 0)
	@Max(value = 1)
	private double granularity;

	@NotNull
	private LocalDate editDate;
	
	private String description;
	
	@LazyCollection(LazyCollectionOption.FALSE)
	@ManyToOne
	@JsonIgnore
	private Milestone milestone;
	
	@LazyCollection(LazyCollectionOption.FALSE)
	@ManyToOne
	@JsonIgnoreProperties({"firstName",
			"lastName",
			"login" ,
			"role",
			"projects",
			"manager"})
	@NotNull
	private Person person;

	
	/**
	 * Clear the person and project links with logs object
	 */
	public void clearLogsLinkBeforeRemoving(){
		if(person != null){
			this.person.getLogs().remove(this);
		}
		if(milestone != null){
			this.milestone.getLogs().remove(this);
		}
	}
}
