package fr.tse.projetpoc.domain;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Class which represents a milestone
 */
@Data
@Entity
@AllArgsConstructor
public class Milestone {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private String description;

	@NotNull
	private LocalDate startDate;

	@NotNull
	private LocalDate endDate;
	
	@ManyToOne
	@JsonIgnore
	@NotNull
	private Project project;
	
	@ManyToOne
	@NotNull
	private MilestoneTitle milestoneTitle;
	
	@LazyCollection(LazyCollectionOption.FALSE)
	@OneToMany(mappedBy = "milestone", cascade={CascadeType.ALL}, orphanRemoval=true)
	@JsonIgnore
	@EqualsAndHashCode.Exclude
	private List<Logs> logs;
	
	// Constructors
	public Milestone() {
		this.logs = new ArrayList<>();
	}
	

	public Milestone(Long id, String description, LocalDate startDate, LocalDate endDate, Project project, MilestoneTitle milestoneTitle) {
		super();
		this.id = id;
		this.description = description;
		this.startDate = startDate;
		this.endDate = endDate;
		this.project = project;
		this.milestoneTitle = milestoneTitle;
		this.logs = new ArrayList<>();
	}
	
	
	
	/**
	 *  Clear entities links with milestone object before removing milestone
	 */
	public void clearEntitiesLinksBeforeRemoving(){
		this.project.getMilestones().remove(this);
		this.logs.forEach(logs -> logs.setMilestone(null));
		this.logs.clear();
	}
	
	/**
	 * Add a logs related to the milestone
	 * @param logs The logs to add
	 * @return The state of the adding operation
	 */
	public boolean addLogs(Logs logs) {
		return this.logs.add(logs);
	}


}
