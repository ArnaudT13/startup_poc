package fr.tse.projetpoc.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import com.sun.istack.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Class which represents a milestone type
 */
@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class MilestoneTitle {
	
	@Id
	private Long id;

	@NotNull
	@Column(unique = true)
	private String title;

}
