package fr.tse.projetpoc.domain;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.NoArgsConstructor;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * Class which represents a contributor (manager, admin, developer)
 */
@Data
@Entity
@AllArgsConstructor
@Table(name = "Person", uniqueConstraints={
		@UniqueConstraint( name = "idx_col1_col2",  columnNames ={"id","login"})
})
public class Person{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@NotEmpty
	private String firstName;

	@NotNull
	@NotEmpty
	private String lastName;

	@NotNull
	@NotEmpty
	private String login;

	@JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
	@NotNull
	private String password;

	@ManyToOne
	@NotNull
	private Role role;


	@ManyToMany(mappedBy = "contributors", fetch=FetchType.EAGER)
	@EqualsAndHashCode.Exclude
	private List<Project> projects;
	
	@LazyCollection(LazyCollectionOption.FALSE)
	@OneToMany(mappedBy = "person", cascade={CascadeType.ALL}, orphanRemoval=true)
	@EqualsAndHashCode.Exclude
	@JsonIgnore
	private List<Logs> logs;
	
	@ManyToOne(optional = true, fetch = FetchType.EAGER)
	@JsonIgnoreProperties(value = { "projects", "logs", "manager" })
	private Person manager;
	
	// Constructors
	public Person() {
		projects = new ArrayList<>();
		logs = new ArrayList<>();
		this.manager = null;
	}
	

	public Person(Long id, String firstName, String lastName, String login, String password, Role role) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.login = login;
		this.password = password;
		this.role = role;
		this.projects = new ArrayList<>();
		this.logs = new ArrayList<>();
		this.manager = null;
	}
	

	/**
	 * Clear the person links with other entities before removing
	 */
	public void clearPersonLinksBeforeRemoving(){
		this.projects.forEach(project -> project.setContributors(null));
		this.projects.clear();
		
		this.logs.forEach(logs -> logs.setPerson(null));
		this.logs.clear();
	}
	
	
	/**
	 * Add manager to the current person
	 */
	public void addOrChangeManager(Person manager) {
		this.manager = manager;
	}
	
	
	/**
	 * Delete manager to the current person
	 */
	public void deleteManager() {
		this.manager = null;
	}
}
