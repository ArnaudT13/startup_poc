package fr.tse.projetpoc.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Class which represents a project
 */
@Data
@Entity
@AllArgsConstructor
public class Project {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@NotEmpty
	private String name;
	
	@LazyCollection(LazyCollectionOption.FALSE)
	@ManyToMany(fetch=FetchType.EAGER)
	@EqualsAndHashCode.Exclude
	@JsonIgnore
	private List<Person> contributors;
	
	@LazyCollection(LazyCollectionOption.FALSE)
	@OneToMany(mappedBy = "project", cascade={CascadeType.ALL})
	@EqualsAndHashCode.Exclude
	private List<Milestone> milestones;
	
	// Constructors
	public Project() {
		contributors = new ArrayList<>();
		milestones = new ArrayList<>();
	}
	
	public Project(Long id, String name) {
		super();
		this.id = id;
		this.name = name;
		this.contributors = new ArrayList<>();
		this.milestones = new ArrayList<>();
	}
	
	
	/**
	 * Add a milestone in the project
	 * @param milestone The milestone to add
	 */
	public void addMilestone(Milestone milestone) {
		milestone.setProject(this);
		this.milestones.add(milestone);
	}
	
	/**
	 * Delete a milestone in the project
	 * @param milestone The milestone to delete
	 */
	public void deleteMilestone(Milestone milestone) {
		milestone.setProject(null);
		this.milestones.remove(milestone);
	}
	
	/**
	 * Add a contributor in the project
	 * @param person The contributor to add
	 */
	public void addContributor(Person person) {
		person.getProjects().add(this);
		this.contributors.add(person);
	}
	
	/**
	 * Delete a contributor in the project
	 * @param person The contributor to delete
	 */
	public void deleteContributor(Person person) {
		person.getProjects().remove(this);
		this.contributors.remove(person);
	}
	
	/**
	 * Clear the project links with other entities before removing
	 */
	public void clearProjectLinksBeforeRemoving() {
		if(this.contributors != null){
			this.contributors.forEach(contributor -> contributor.getProjects().remove(this));
			this.contributors.clear();
		}
		
		if(this.milestones != null) {
			this.milestones.forEach(milestone -> milestone.setProject(null));
			this.milestones.clear();
		}	
	}
}
