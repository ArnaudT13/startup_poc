package fr.tse.projetpoc.domain;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import fr.tse.projetpoc.utils.RoleType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;


/**
 * Class which represents a project role
 */
@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class Role implements Serializable {
	@Id
	private Long id;

	@NotNull
	@NotEmpty
	@Enumerated(EnumType.STRING)
	private RoleType label;
}
