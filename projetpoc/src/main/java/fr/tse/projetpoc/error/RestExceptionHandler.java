package fr.tse.projetpoc.error;

import io.swagger.annotations.ApiOperation;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import fr.tse.projetpoc.exception.BadArgumentException;
import fr.tse.projetpoc.exception.UserLoginAlreadyExists;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    private ResponseEntity<Object> buildResponseEntity(ApiError apiError) {
        return new ResponseEntity<>(apiError, apiError.getStatus());
    }

    @ApiOperation("Send if a BadArgumentException occurred.")
    @ExceptionHandler({BadArgumentException.class ,DataIntegrityViolationException.class})
    protected ResponseEntity<Object> handleBadArgument(Exception ex) {
        ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST, ex.getMessage(), "ARG_ERR");
        return buildResponseEntity(apiError);
    }

    @ApiOperation("Send if a UsernameNotFoundException occurred.")
    @ExceptionHandler(UsernameNotFoundException.class)
    protected ResponseEntity<Object> handleAuthException(Exception ex) {
        ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST, ex.getMessage(), "AUTH_ERR");
        return buildResponseEntity(apiError);
    }
    @ApiOperation("Send if a UserLoginAlreadyExists occurred.")
    @ExceptionHandler(UserLoginAlreadyExists.class)
    protected ResponseEntity<Object> handleUserLoginException(Exception ex) {
        ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST, ex.getMessage(), "LOGIN_ERR");
        return buildResponseEntity(apiError);
    }

    @ApiOperation("Send if an Exception not handled occurred.")
    @ExceptionHandler(Exception.class)
    protected ResponseEntity<Object> handleBadException(Exception ex) {
        ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST, ex.getMessage(), "EXC_ERR");
        return buildResponseEntity(apiError);
    }

}
