package fr.tse.projetpoc.exception;

public class BadArgumentException extends Exception {
    public BadArgumentException(String message) {
        super(message);
    }
}
