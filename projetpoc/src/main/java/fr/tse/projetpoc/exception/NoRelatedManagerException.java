package fr.tse.projetpoc.exception;

public class NoRelatedManagerException extends Exception{
	public NoRelatedManagerException(String message) {
		super(message);
	}
}
