package fr.tse.projetpoc.exception;

public class NotManagerException extends Exception{
	public NotManagerException(String message) {
		super(message);
	}
}
