package fr.tse.projetpoc.exception;

public class UserLoginAlreadyExists extends Exception{
	public UserLoginAlreadyExists(String message) {
		super(message);
	}
}

