package fr.tse.projetpoc.security;

import fr.tse.projetpoc.dao.PersonRepository;
import fr.tse.projetpoc.domain.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    @Autowired
    PersonRepository personRepository;
    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        Person person = this.personRepository.findByLogin(s).get(0);
        if( person == null){
            throw new UsernameNotFoundException("User not found");
        }
        return new UserDetailsImpl(person);
    }


}
