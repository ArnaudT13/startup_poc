package fr.tse.projetpoc.service.Impl;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import fr.tse.projetpoc.domain.Milestone;
import fr.tse.projetpoc.exception.BadArgumentException;
import fr.tse.projetpoc.service.MilestoneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.tse.projetpoc.dao.LogsRepository;
import fr.tse.projetpoc.domain.Logs;
import fr.tse.projetpoc.domain.Person;
import fr.tse.projetpoc.domain.Project;
import fr.tse.projetpoc.service.LogsService;
import javassist.tools.rmi.ObjectNotFoundException;

/**
 * Implementation of the logs service
 */
@Service
public class LogsServiceImpl implements LogsService {
	
    @Autowired
    LogsRepository logsRepository;
    @Autowired
    MilestoneService milestoneService;
    
    
    @Override
    @Transactional
    public List<Logs> findAllLogs() {
        return this.logsRepository.findAll();
    }

    @Override
    @Transactional
    public Logs findLogsById(Long id) {
        return this.logsRepository.findById(id).orElse(null);
    }

    @Override
    @Transactional
    public List<Logs> findLogsByProject(Project project) {
    	List<Logs> logsList = new ArrayList<>();
    	for (Logs logs : this.logsRepository.findAll()) {
    		if (logs.getMilestone().getProject().getId() == project.getId()) {
    			logsList.add(logs);
    		}
    	}
        return logsList;
    }

    @Override
    @Transactional
    public List<Logs> findLogsByPerson(Person person) {
        return this.logsRepository.findByPerson(person);
    }

    @Override
    @Transactional
    public List<Logs> findLogsByPersonAndProject(Person person,Project project){
    	List<Logs> logsList = new ArrayList<>();
    	for (Logs logs : this.logsRepository.findAll()) {
    		if (logs.getMilestone().getProject().getId() == project.getId() && logs.getPerson().getId() == person.getId()) {
    			logsList.add(logs);
    		}
    	}
        return logsList;
    }

    @Override
    @Transactional
    public Logs addLogs(Logs logs) {
        return this.logsRepository.save(logs);
    }

    @Override
    @Transactional
    public void removeLogs(Logs logs) throws ObjectNotFoundException {
        if(logs != null && this.findLogsById(logs.getId()) != null){
        	logs = this.logsRepository.save(logs);
            logs.clearLogsLinkBeforeRemoving();
            this.logsRepository.delete(logs);
        }else{
            throw new ObjectNotFoundException("The Log doesn't exist");
        }
    }

    @Override
    public Milestone getMilestoneFromLogs(Long id) throws BadArgumentException {
        Logs logs = this.findLogsById(id);
        if(logs != null){
            return logs.getMilestone();
        }else{
            throw new BadArgumentException("The Log doesn't exist");
        }
    }

    @Override
    public Logs modLogs(Long id, LocalDate editDate, Double granularity, String description) throws BadArgumentException {
        Logs logs = this.findLogsById(id);
        if(logs != null){
            logs.setDescription(description);
            logs.setGranularity(granularity);
            logs.setEditDate(editDate);
            return this.logsRepository.save(logs);
        }else{
            throw new BadArgumentException("The Log doesn't exist");
        }
    }

    @Override
    public Project getProjectFromLogs(Long id) throws BadArgumentException {
        Logs logs = this.findLogsById(id);
        if(logs != null){
            return this.milestoneService.findProjectFromMilestone(logs.getMilestone().getId());
        }else{
            throw new BadArgumentException("The Log doesn't exist");
        }
    }
}
