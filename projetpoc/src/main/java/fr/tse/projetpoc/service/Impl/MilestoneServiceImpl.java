package fr.tse.projetpoc.service.Impl;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import fr.tse.projetpoc.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.tse.projetpoc.dao.LogsRepository;
import fr.tse.projetpoc.dao.MilestoneRepository;
import fr.tse.projetpoc.dao.PersonRepository;
import fr.tse.projetpoc.dao.ProjectRepository;
import fr.tse.projetpoc.domain.Logs;
import fr.tse.projetpoc.domain.Milestone;
import fr.tse.projetpoc.domain.Person;
import fr.tse.projetpoc.domain.Project;
import fr.tse.projetpoc.exception.BadArgumentException;
import fr.tse.projetpoc.service.LogsService;
import fr.tse.projetpoc.service.MilestoneService;
import javassist.tools.rmi.ObjectNotFoundException;

/**
 * Implementation of the milestone service
 */
@Service
public class MilestoneServiceImpl implements MilestoneService {
	
    @Autowired
    MilestoneRepository milestoneRepository;
    
    @Autowired
    ProjectRepository projectRepository;
	@Autowired
	ProjectService projectService;
    
    @Autowired
    LogsRepository logsRepository;
    
    @Autowired
    PersonRepository personRepository;
    
    @Autowired
    LogsService logsService;
    
	@Override
	public List<Milestone> findAllMilestones() {
		return this.milestoneRepository.findAll();
	}

    @Override
    @Transactional
    public List<Milestone> findMilestonesByProject(Project project) {
       return this.milestoneRepository.findMilestonesByProject(project);
    }
    
	@Override
	@Transactional
	public List<Milestone> findMilestonesByDescription(String description) {
		return this.milestoneRepository.findMilestonesByDescription(description);
	}

	@Override
	public Project findProjectFromMilestone(Long id) throws BadArgumentException {
		Milestone milestone = this.findMilestoneById(id);
		if(milestone != null){
			return milestone.getProject();
		}else{
			throw new BadArgumentException("The milestone doesn't exist.");
		}
	}

	@Override
    @Transactional
    public Milestone addMilestone(Long projectId,Milestone milestone) throws BadArgumentException {
        Milestone milestoneRes = this.milestoneRepository.save(milestone);
        if(this.projectService.findProjectById(projectId) != null){
			this.projectService.findProjectById(projectId).addMilestone(milestoneRes);
		}
        return milestoneRes;
    }

    @Override
    @Transactional
    public Milestone findMilestoneById(Long id) {
        return this.milestoneRepository.findById(id).orElse(null);
    }

    @Override
    @Transactional
    public void removeMilestone(Milestone milestone) throws ObjectNotFoundException {
        
    	try {
        	milestone = this.milestoneRepository.save(milestone); // save in separate transaction
    		
        	milestone.clearEntitiesLinksBeforeRemoving();
        	
            this.milestoneRepository.delete(milestone);
    	} catch (Exception e) {
    		throw new ObjectNotFoundException("The milestone doesn't exist.");
    	}
    }

	@Override
	@Transactional
	public Milestone addLogsInMilestone(Long milestoneId, Logs logs, Person person) throws BadArgumentException,ObjectNotFoundException {
		// Get project from id or return exception
		Milestone milestone = this.milestoneRepository.findById(milestoneId).orElseThrow(() -> new BadArgumentException("Wrong id argument"));

		if(person == null)
		    throw new ObjectNotFoundException("Person is null");
		// Add logs
		logs.setPerson(person);
		logs.setMilestone(milestone);
		milestone.addLogs(logs);
		// Save milestone
		return milestoneRepository.save(milestone);
	}
	
	
	@Override
	@Transactional
	public void addLogsToMilestone(Milestone milestone, Person person, double granularity, LocalDate editDate, String description) throws ObjectNotFoundException {
		if(milestone != null && this.findMilestoneById(milestone.getId()) != null){
			if(person != null && this.personRepository.findById(person.getId()).orElse(null) != null){
				Logs logs = new Logs(null, granularity, editDate, description, null, null);
				logs.setMilestone(this.findMilestoneById(milestone.getId()));
				logs.setPerson(this.personRepository.findById(person.getId()).orElse(null));
				logsService.addLogs(logs);
	        }else{
	            throw new ObjectNotFoundException("The person doesn't exist.");
	        }
        }else{
            throw new ObjectNotFoundException("The milestone doesn't exist.");
        }
	}
}
