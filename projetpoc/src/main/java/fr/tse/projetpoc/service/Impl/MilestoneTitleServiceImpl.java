package fr.tse.projetpoc.service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.tse.projetpoc.dao.MilestoneTitleRepository;
import fr.tse.projetpoc.domain.MilestoneTitle;
import fr.tse.projetpoc.service.MilestoneTitleService;

/**
 * Implementation of the milestone title service
 */
@Service
public class MilestoneTitleServiceImpl implements MilestoneTitleService {
	
    @Autowired
    MilestoneTitleRepository milestoneTitleRepository;
    
    
    @Override
    @Transactional
    public List<MilestoneTitle> findAllMilestonesTitle() {
        return this.milestoneTitleRepository.findAll();
    }

    @Override
    @Transactional
    public MilestoneTitle findMilestonesTitleById(Long id) {
        return this.milestoneTitleRepository.findById(id).orElse(null);
    }

    @Override
    @Transactional
    public MilestoneTitle addMilestoneTitle(MilestoneTitle milestoneTitle) throws DataIntegrityViolationException {
        return this.milestoneTitleRepository.save(milestoneTitle);
    }
}
