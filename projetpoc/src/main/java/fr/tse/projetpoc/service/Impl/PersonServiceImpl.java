package fr.tse.projetpoc.service.Impl;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.tse.projetpoc.dao.MilestoneRepository;
import fr.tse.projetpoc.dao.PersonRepository;
import fr.tse.projetpoc.domain.Logs;
import fr.tse.projetpoc.domain.Milestone;
import fr.tse.projetpoc.domain.Person;
import fr.tse.projetpoc.domain.Project;
import fr.tse.projetpoc.domain.Role;
import fr.tse.projetpoc.exception.NotManagerException;
import fr.tse.projetpoc.exception.UserLoginAlreadyExists;
import fr.tse.projetpoc.service.LogsService;
import fr.tse.projetpoc.service.PersonService;
import fr.tse.projetpoc.service.ProjectService;
import fr.tse.projetpoc.service.RoleService;
import fr.tse.projetpoc.utils.Constants;
import javassist.tools.rmi.ObjectNotFoundException;

/**
 * Implementation of the person service
 */
@Slf4j
@Service
public class PersonServiceImpl implements PersonService {

    @Autowired
    PersonRepository personRepository;
    
    @Autowired
    MilestoneRepository milestoneRepository;

    @Autowired
    RoleService roleService;

    @Autowired
    ProjectService projectService;

    @Autowired
    LogsService logsService;

    @Override
    @Transactional
    public List<Person> findAllPerson() {
        return this.personRepository.findAll();
    }

    @Override
    @Transactional
    public Person findPersonById(Long id) {
        return this.personRepository.findById(id).orElse(null);
    }

    @Override
    @Transactional
    public List<Person> findPersonsByFirstName(String firstname) {
        return this.personRepository.findByFirstName(firstname);
    }

    @Override
    @Transactional
    public List<Person> findPersonsByLastName(String lastname) {
        return this.personRepository.findByLastName(lastname);
    }
    
    @Override
    @Transactional
    public List<Person> findPersonsByUsername(String username) {
        return this.personRepository.findByLogin(username);
    }

    @Override
    @Transactional
    public Person addPerson(Person person) throws UserLoginAlreadyExists {
    	List<Person> persons = this.personRepository.findAll();
    	
    	for (Person p : persons) {
    		if(p.getLogin().equals(person.getLogin())) {
    			System.out.println(p.getLogin() + " " + person.getLogin());
    			throw new UserLoginAlreadyExists("The user login already exists in database.");
    		}
    	}
        return this.personRepository.save(person);
    }

    @Override
    @Transactional
    public void deletePerson(Long id) throws ObjectNotFoundException {
    	Person person = this.findPersonById(id);
        if(person != null && this.findPersonById(id) != null){
        	person = this.personRepository.save(person);
            person.clearPersonLinksBeforeRemoving();
            this.personRepository.delete(person);
        }else{
            throw new ObjectNotFoundException("The person doesn't exist.");
        }
    }

    @Override
    @Transactional
    public Person modifyRole(Person person, Role role) throws ObjectNotFoundException {
        if(person != null && this.findPersonById(person.getId()) != null){
            if(this.roleService.findRoleById(role.getId()) != null){
                person.setRole(role);
                return this.personRepository.save(person);
            }else {
                throw new ObjectNotFoundException("The role doesn't exist");
            }
        }else{
            throw new ObjectNotFoundException("The person doesn't exist.");
        }
    }

	@Override
	@Transactional
	public void addOrChangeManager(Person person, Person manager) throws ObjectNotFoundException, NotManagerException {
		if(person != null && this.findPersonById(person.getId()) != null){
			if(manager != null && this.findPersonById(manager.getId()) != null){
				if (manager.getRole().getId() == Constants.ROLE_MANAGER_ID) {
					person.addOrChangeManager(manager);
					this.personRepository.save(person);
				}else {
					 throw new NotManagerException("The manger object has no manager role");
				}
	        }else{
	            throw new ObjectNotFoundException("The manager doesn't exist.");
	        }
        }else{
            throw new ObjectNotFoundException("The person doesn't exist.");
        }
	}

	@Override
	@Transactional
	public void deleteManager(Person person) throws ObjectNotFoundException {
		if(person != null && this.findPersonById(person.getId()) != null){
			person.deleteManager();
			this.personRepository.save(person);
        }else{
            throw new ObjectNotFoundException("The person doesn't exist.");
        }
	}
	
	@Override
	@Transactional
	public void addLogsToUser(Person person, Milestone milestone, double granularity, LocalDate editDate, String description) throws ObjectNotFoundException {
		if(person != null && this.findPersonById(person.getId()) != null){
			if(milestone != null && this.milestoneRepository.findById(milestone.getId()).orElse(null) != null){
				Logs logs = new Logs(null, granularity, editDate, description, milestone, person);
				logsService.addLogs(logs);
	        }else{
	            throw new ObjectNotFoundException("The milestone doesn't exist.");
	        }
        }else{
            throw new ObjectNotFoundException("The person doesn't exist.");
        }
	}
	
	@Override
	@Transactional
	public List<Person> findAllDeveloperForManager(Person manager) throws ObjectNotFoundException, NotManagerException {
		if(manager != null && this.findPersonById(manager.getId()) != null){
			if (manager.getRole().getId() == Constants.ROLE_MANAGER_ID) {
				List<Person> personList = new ArrayList<>();
				for (Person developer : this.personRepository.findAll()) {
					if (developer.getManager() != null && developer.getManager().getId() == manager.getId()) {
						personList.add(developer);
					}
				}
				return personList;
			}else {
				 throw new NotManagerException("The manger object has no manager role");
			}
        }else{
            throw new ObjectNotFoundException("The person doesn't exist.");
        }
	}
	
	@Override
	@Transactional
	public List<Project> findAllProjectForPerson(Person person) throws ObjectNotFoundException {
		if(person != null && this.findPersonById(person.getId()) != null){
			return person.getProjects();
        }else{
            throw new ObjectNotFoundException("The person doesn't exist.");
        }
	}
}
