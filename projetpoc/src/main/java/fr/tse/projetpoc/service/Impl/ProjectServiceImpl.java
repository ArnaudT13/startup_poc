package fr.tse.projetpoc.service.Impl;

import java.util.ArrayList;
import java.util.List;

import fr.tse.projetpoc.dao.MilestoneTitleRepository;
import fr.tse.projetpoc.domain.MilestoneTitle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.tse.projetpoc.dao.MilestoneRepository;
import fr.tse.projetpoc.dao.ProjectRepository;
import fr.tse.projetpoc.domain.Milestone;
import fr.tse.projetpoc.domain.Person;
import fr.tse.projetpoc.domain.Project;
import fr.tse.projetpoc.exception.BadArgumentException;
import fr.tse.projetpoc.service.MilestoneService;
import fr.tse.projetpoc.service.ProjectService;
import javassist.tools.rmi.ObjectNotFoundException;

/**
 * Implementation of the project service
 */
@Service
public class ProjectServiceImpl implements ProjectService {

    @Autowired
    ProjectRepository projectRepository;
    
    @Autowired
    MilestoneRepository milestoneRepository;
    @Autowired
    MilestoneTitleRepository milestoneTitleRepository;
    @Autowired
    MilestoneService milestoneService;

    @Override
    @Transactional
    public List<Project> findAllProject() {
        return this.projectRepository.findAll();
    }

    @Override
    @Transactional
    public Project findProjectById(Long id) {
        return this.projectRepository.findById(id).orElse(null);
    }

    @Override
    @Transactional
    public List<Project> findProjectsByName(String name) {
        return this.projectRepository.findByName(name);
    }

    @Override
    @Transactional
    public Project addProject(Project project) {
        Project proj = this.projectRepository.save(project);
        for(Milestone m:proj.getMilestones()){
            m.setProject(proj);
        }
        return proj;
    }

    @Override
    @Transactional
    public void deleteProject(Project project) throws ObjectNotFoundException {
        if(project != null && this.findProjectById(project.getId()) != null){
	    	project = this.projectRepository.save(project);
	    	project.clearProjectLinksBeforeRemoving();       	    	
	        this.projectRepository.delete(project);
        }else{
            throw new ObjectNotFoundException("The project doesn't exist");
        }
    }

	@Override
	@Transactional
	public Project addMilestoneInProject(Long projectId, Milestone milestone) throws BadArgumentException {
		// Get project from id or return exception
		Project project = this.projectRepository.findById(projectId).orElseThrow(() -> new BadArgumentException("Wrong id argument"));

		// Add milestone
		project.addMilestone(milestone);

		// Save project
		return projectRepository.save(project);
	}

	@Override
	@Transactional
	public Project deleteMilestoneInProject(Long projectId, Milestone milestone) throws BadArgumentException, ObjectNotFoundException {
		// Get project from id or return exception
		Project project = this.projectRepository.findById(projectId).orElseThrow(() -> new BadArgumentException("Wrong id argument"));

		// Delete milestone
		milestoneService.removeMilestone(milestone);
		//project.deleteMilestone(milestone);
		
		// Save project
		return projectRepository.save(project);
	}

	@Override
	@Transactional
	public Project addContributorInProject(Long projectId, Person person) throws BadArgumentException {
		// Get project from id or return exception
		Project project = this.projectRepository.findById(projectId).orElseThrow(() -> new BadArgumentException("Wrong id argument"));

		// Add person
		project.addContributor(person);

		// Save project
		return projectRepository.save(project);
	}

	@Override
	@Transactional
	public Project deleteContributorInProject(Long projectId, Person person) throws BadArgumentException {
		// Get project from id or return exception
		Project project = this.projectRepository.findById(projectId).orElseThrow(() -> new BadArgumentException("Wrong id argument"));

		// Delete person
		project.deleteContributor(person);

		// Save project
		return projectRepository.save(project);
	}

    @Override
    public List<Person> getContributor(Long projectId) throws BadArgumentException {
        Project project = this.projectRepository.findById(projectId).orElse(null);
        if(project != null){
            return project.getContributors();
        }else{
            throw new BadArgumentException("Project id doesn't exist");
        }
    }

}
