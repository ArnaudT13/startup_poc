package fr.tse.projetpoc.service.Impl;

import fr.tse.projetpoc.dao.PersonRepository;
import fr.tse.projetpoc.exception.BadArgumentException;
import fr.tse.projetpoc.service.ReportService;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePdfExporterConfiguration;
import net.sf.jasperreports.export.SimplePdfReportConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import java.io.*;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

@Service
public class ReportServiceImpl implements ReportService {
    @Autowired
    Environment env;
    @Autowired
    PersonRepository personRepository;
    @Override
    public JasperPrint getReport(Long personId, LocalDate startDate, LocalDate endDate) throws BadArgumentException, SQLException, JRException {
        Map<String,Object> parameters = new HashMap<>();
        parameters.put("Person_id",personId);
        parameters.put("Start_date",java.sql.Date.valueOf(startDate));
        parameters.put("End_date",java.sql.Date.valueOf(endDate));
        if(this.personRepository.findById(personId).orElse(null) == null){
            throw new BadArgumentException("The person doesn't exist.");
        }
        URL res = getClass().getClassLoader().getResource("reports/person_report.jasper");
        File file = null;
		try {
			file = Paths.get(res.toURI()).toFile();
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
        String absolutePath = file.getAbsolutePath();

        String path = absolutePath;
        String url = env.getProperty("spring.datasource.url");
        String username = env.getProperty("spring.datasource.username");
        String password = env.getProperty("spring.datasource.password");

        Connection conn = DriverManager.getConnection(url,username,password);

        JasperPrint jasperPrint = JasperFillManager.fillReport(path,parameters,conn);
        return jasperPrint;
    }
}
