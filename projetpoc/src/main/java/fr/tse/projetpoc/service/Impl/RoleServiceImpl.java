package fr.tse.projetpoc.service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.tse.projetpoc.dao.RoleRepository;
import fr.tse.projetpoc.domain.Role;
import fr.tse.projetpoc.service.RoleService;

/**
 * Implementation of the role service
 */
@Service
public class RoleServiceImpl implements RoleService {
	
    @Autowired
    RoleRepository roleRepository;
    
    
    @Override
    @Transactional
    public List<Role> findAllRole() {
        return this.roleRepository.findAll();
    }

    @Override
    @Transactional
    public Role findRoleById(Long id) {
        return this.roleRepository.findById(id).orElse(null);
    }
}
