package fr.tse.projetpoc.service;

import java.time.LocalDate;
import java.util.List;

import fr.tse.projetpoc.domain.Logs;
import fr.tse.projetpoc.domain.Milestone;
import fr.tse.projetpoc.domain.Person;
import fr.tse.projetpoc.domain.Project;
import fr.tse.projetpoc.exception.BadArgumentException;
import javassist.tools.rmi.ObjectNotFoundException;


/**
 * The logs service interface
 */
public interface LogsService {
	
	/**
	 * Retrieve all logs in database
	 * @return The list of logs in database
	 */
    public List<Logs> findAllLogs();
    
    /**
     * Retrieve logs object according to the id parameter
     * @param id The logs id in database
     * @return The logs object corresponding to the id in database
     */
    public Logs findLogsById(Long id);
    
    /**
     * Retrieve all logs corresponding to the project parameter
     * @param project The project object in database
     * @return The list of logs in database
     */
    public List<Logs> findLogsByProject(Project project);
    
    /**
     * Retrieve all logs corresponding to the person parameter
     * @param person The person object in database
     * @return The list of logs in database
     */
    public List<Logs> findLogsByPerson(Person person);
    
    /**
     * Retrieve all logs corresponding to the person and project parameters
     * @param person The person object in database
     * @param project The project object in database
     * @return The list of logs in database
     */
    public List<Logs> findLogsByPersonAndProject(Person person, Project project);


    /**
     * Add logs to database
     * @param logs the logs you want to save
     * @return logs saved
     */
    public Logs addLogs(Logs logs);
    /**
     * Remove logs object passed as argument
     * @param logs The logs object in database
     * @throws ObjectNotFoundException The logs doesn't exist in database
     */
    public void removeLogs(Logs logs) throws ObjectNotFoundException;

    /**
     * Retrieves the milestone of the logs
     * @param id the log id
     * @return Milestone
     * @throws BadArgumentException the logs' id doesn't exist
     */
    public Milestone getMilestoneFromLogs(Long id) throws BadArgumentException;

    /**
     * Modifiy the logs
     * @param id the log to modify
     * @param editDate the new edition date
     * @param granularity the new granularity
     * @param description the new description
     * @return the new logs
     * @throws BadArgumentException the logs' id doesn't exist
     */
    public Logs modLogs(Long id, LocalDate editDate, Double granularity, String description) throws  BadArgumentException;
    /**
     * Retrieves the project of a log
     * @param id the log id
     * @return the project
     * @throws BadArgumentException the logs' id doesn't exist
     */
    public Project getProjectFromLogs(Long id) throws BadArgumentException;
}
