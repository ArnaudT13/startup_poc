package fr.tse.projetpoc.service;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import fr.tse.projetpoc.domain.Logs;
import fr.tse.projetpoc.domain.Milestone;
import fr.tse.projetpoc.domain.Person;
import fr.tse.projetpoc.domain.Project;
import fr.tse.projetpoc.exception.BadArgumentException;
import javassist.tools.rmi.ObjectNotFoundException;

/**
 * The milestone service interface
 */
public interface MilestoneService {
	
	/**
	 * Retrieve all milestones in database
	 * @return The list of milestones in database
	 */
	public List<Milestone> findAllMilestones();
	
    /**
     * Retrieve milestone object according to the id parameter
     * @param id The milestone id in database
     * @return The milestone object corresponding to the id in database
     */
    public Milestone findMilestoneById(Long id);
	
    /**
     * Retrieve all milestones corresponding to the project parameter
     * @param project The project object in database
     * @return The milestones in database
     */
    public List<Milestone> findMilestonesByProject(Project project);
    
    /**
     * Retrieve all milestones corresponding to the description parameter
     * @param description The milestone description
     * @return The milestones in database
     */
    public List<Milestone> findMilestonesByDescription(String description);

    /**
     * Retrieves project of the milestone
     * @param id the milestone id
     * @return Project
     * @throws BadArgumentException The milestone's id doesn't exist
     */
    public Project findProjectFromMilestone(Long id)throws BadArgumentException;
    /**
     * Add milestone to database
     * @param projectId The id of the milestone's project
     * @param milestone The milestone you want to add
     * @return The milestone saved
     */
    public Milestone addMilestone(Long projectId, Milestone milestone) throws BadArgumentException;
    /**
     * Remove milestone object passed as argument
     * @param milestone The milestone object in database
     * @throws ObjectNotFoundException The milestone doesn't exist in database
     */
    public  void removeMilestone(Milestone milestone) throws ObjectNotFoundException;
    
    /**
     * Add a logs in milestone object
     * @param milestoneId The id of the milestone in database
     * @param logs The logs to add in the milestone object
     * @param person The person related to the logs
     * @return The project object saved in database
     * @throws BadArgumentException The milestone id doesn't exist
     * @throws ObjectNotFoundException The person doesn't exist
     */
    public Milestone addLogsInMilestone(Long milestoneId, Logs logs, Person person) throws BadArgumentException, ObjectNotFoundException;

    /**
     * Add a logs to the milestone according to the given parameters 
     * @param milestone The milestone of the logs
     * @param person The person to add a logs
     * @param granularity The granularity logs property
     * @param editDate The editDate logs property
     * @param description The description logs property
     * @throws ObjectNotFoundException The person or the milestone don't exist in database
     */
	void addLogsToMilestone(Milestone milestone, Person person, double granularity, LocalDate editDate, String description) throws ObjectNotFoundException;

}
