package fr.tse.projetpoc.service;

import java.util.List;

import org.springframework.dao.DataIntegrityViolationException;

import fr.tse.projetpoc.domain.MilestoneTitle;

/**
 * The milestone title service interface
 */
public interface MilestoneTitleService {
	
	/**
	 * Retrieve all milestones titles in database
	 * @return The list of milestones titles in database
	 */
    public List<MilestoneTitle> findAllMilestonesTitle();
    
    /**
     * Retrieve milestone title object according to the id parameter
     * @param id The milestone title id in database
     * @return The milestone title object corresponding to the id in database
     */
    public MilestoneTitle findMilestonesTitleById(Long id);
    
    /**
     * Add a milestone title in database
     * @param milestoneTitle The milestone title object to insert in database
     * @return The milestone title object saved in database
     * @throws DataIntegrityViolationException the label of the MilestoneTitle already exists in the database
     */
    public MilestoneTitle addMilestoneTitle(MilestoneTitle milestoneTitle) throws DataIntegrityViolationException;
}
