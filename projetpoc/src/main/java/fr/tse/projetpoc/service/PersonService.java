package fr.tse.projetpoc.service;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import fr.tse.projetpoc.domain.Milestone;
import fr.tse.projetpoc.domain.Person;
import fr.tse.projetpoc.domain.Project;
import fr.tse.projetpoc.domain.Role;
import fr.tse.projetpoc.exception.NotManagerException;
import fr.tse.projetpoc.exception.UserLoginAlreadyExists;
import javassist.tools.rmi.ObjectNotFoundException;
import org.springframework.security.access.prepost.PreAuthorize;

/**
 * The person service interface
 */
public interface PersonService {

	/**
	 * Retrieve all person in database
	 * @return The list of person in database
	 */
    public List<Person> findAllPerson();

    /**
     * Retrieve person object according to the id parameter
     * @param id The person id in database
     * @return The person object corresponding to the id in database
     */
    public Person findPersonById(Long id);

    /**
     * Retrieve person object according to the firstname parameter
     * @param firstname The person firstname to search in database
     * @return The list of person corresponding to the firstName in database
     */
    public List<Person> findPersonsByFirstName(String firstname);

    /**
     * Retrieve person object according to the lastname parameter
     * @param lastname The person lastname to search in database
     * @return The list of person corresponding to the lastname in database
     */
    public List<Person> findPersonsByLastName(String lastname);
    
    /**
     * Retrieve person object according to the username parameter
     * @param username  The person username to search in database
     * @return The list of person corresponding to the username in database
     */
	public List<Person> findPersonsByUsername(String username);

    /**
     * Add a person in database
     * @param person The person object to insert in database
     * @return The person object saved in database
     * @throws UserLoginAlreadyExists The user login already exists in database
     */
    public Person addPerson(Person person) throws UserLoginAlreadyExists;

    /**
     * Delete a person in database
     * @param id The id of the person to delete in database
     * @throws ObjectNotFoundException The person doesn't exist in database
     */
    public void deletePerson(Long id) throws ObjectNotFoundException;

    /**
     * Modify the @Override
	role of a person passed as parameter
     * @param person The person object in database
     * @param role The new person role object
     * @return The person object saved in database
     * @throws ObjectNotFoundException The person or the role don't exist in database
     */
    public Person modifyRole(Person person, Role role) throws ObjectNotFoundException;
    
    /**
     * Add or change the manager of the person parameter
     * @param person The person to add or change the manager
     * @param manager The manager to affect to the person
     * @throws ObjectNotFoundException The person or the manager don't exist in database
     * @throws NotManagerException The manager object has no manager role
     */
    public void addOrChangeManager(Person person, Person manager) throws ObjectNotFoundException, NotManagerException;
    
    /**
     * Delete the manager of the person parameter
     * @param person  The person to delete the manager
     * @throws ObjectNotFoundException The person doesn't exist in database
     */
    public void deleteManager(Person person) throws ObjectNotFoundException;

    /**
     * Add a logs to the person according to the given parameters 
     * @param person The person to add a logs
     * @param milestone The milestone of the logs
     * @param granularity The granularity logs property
     * @param editDate The editDate logs property
     * @param description The description logs property
     * @throws ObjectNotFoundException The person or the milestone don't exist in database
     */
	public void addLogsToUser(Person person, Milestone milestone, double granularity, LocalDate editDate, String description) throws ObjectNotFoundException;

	/**
	 * Retrieve all person in database which have the manager parameter as manager
	 * @param manager The manager in database
	 * @return The person list corresponding to the manager in database
	 * @throws ObjectNotFoundException The manager doesn't exist in database
	 * @throws NotManagerException The manager object has no manager role
	 */
	public List<Person> findAllDeveloperForManager(Person manager) throws ObjectNotFoundException, NotManagerException;

	
	/**
	 * Retrieve all projects in database for a person
	 * @param person The person to inspect
	 * @return The person project list
	 * @throws ObjectNotFoundException The person doesn't exist in the database
	 */
	public List<Project> findAllProjectForPerson(Person person) throws ObjectNotFoundException;

}
