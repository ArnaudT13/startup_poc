package fr.tse.projetpoc.service;

import java.util.List;

import fr.tse.projetpoc.domain.Milestone;
import fr.tse.projetpoc.domain.Person;
import fr.tse.projetpoc.domain.Project;
import fr.tse.projetpoc.exception.BadArgumentException;
import javassist.tools.rmi.ObjectNotFoundException;
import org.springframework.security.access.prepost.PreAuthorize;

/**
 * The project service interface
 */
public interface ProjectService {

	/**
	 * Retrieve all projects in database
	 * @return The list of projects in database
	 */
    public List<Project> findAllProject();

    /**
     * Retrieve project object according to the id parameter
     * @param id The project id in database
     * @return The project object corresponding to the id in database
     */
    public Project findProjectById(Long id);

    /**
     * Retrieve all projects corresponding to the project name parameter
     * @param name The projects name to search in database
     * @return The list of projects in database
     */
    public List<Project> findProjectsByName(String name);

    /**
     * Add a project in database
     * @param project The project object to insert in database
     * @return The project object saved in database
     */
    public Project addProject(Project project);

    /**
     * Delete a project in database
     * @param project The project object to delete in database
     * @throws ObjectNotFoundException The person doesn't exist in database
     */
    public void deleteProject(Project project) throws ObjectNotFoundException;

    /**
     * Add a milestone in project object
     * @param projectId The id of the project in database
     * @param milestone The milestone to add in the project object
     * @return The project object saved in database
     */
    public Project addMilestoneInProject(Long projectId, Milestone milestone) throws BadArgumentException;

    /**
     * Delete a milestone in project object
     * @param projectId The id of the project in database
     * @param milestone The milestone to delete in the project object
     * @return The project object saved in database
     * @throws ObjectNotFoundException The milestone doesn't exist
     */
    public Project deleteMilestoneInProject(Long projectId, Milestone milestone) throws BadArgumentException, ObjectNotFoundException;

    /**
     * Add a contributor in project object
     * @param projectId The id of the project in database
     * @param person The contributor to add in the project object
     * @return The project object saved in database
     */
    public Project addContributorInProject(Long projectId, Person person) throws BadArgumentException;

    /**
     * Delete a contributor in project object
     * @param projectId The id of the project in database
     * @param person The contributor to delete in the project object
     * @return The project object saved in database
     */
    public Project deleteContributorInProject(Long projectId, Person person) throws BadArgumentException;


    public List<Person> getContributor(Long projectId) throws  BadArgumentException;

}
