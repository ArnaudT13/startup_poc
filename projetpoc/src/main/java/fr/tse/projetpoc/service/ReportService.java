package fr.tse.projetpoc.service;

import fr.tse.projetpoc.exception.BadArgumentException;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperPrint;

import java.io.FileNotFoundException;
import java.sql.Date;
import java.sql.SQLException;
import java.time.LocalDate;

public interface ReportService {
    /**
     * Return a jasperPrint report of a user between two date. Can be used to create a pdf
     * @param personId the id of the person
     * @param startDate the date from which the report will start
     * @param endDate the end date of the report
     * @return a jasperPrint representing the report
     * @throws BadArgumentException the person's id doesn't exist
     * @throws SQLException error while making query inside the report with the database
     * @throws JRException error while filling the report
     */
    public JasperPrint getReport(Long personId, LocalDate startDate, LocalDate endDate) throws BadArgumentException, SQLException, JRException;
}
