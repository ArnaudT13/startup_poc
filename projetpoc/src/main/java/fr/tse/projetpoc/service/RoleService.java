package fr.tse.projetpoc.service;

import java.util.List;

import fr.tse.projetpoc.domain.Role;

/**
 * The role service interface
 */
public interface RoleService {
	
	/**
	 * Retrieve all roles in database
	 * @return The list of roles in database
	 */
    public List<Role> findAllRole();
    
    /**
     * Retrieve role object according to the id parameter
     * @param id The role id in database
     * @return The role object corresponding to the id in database
     */
    public Role findRoleById(Long id);
}
