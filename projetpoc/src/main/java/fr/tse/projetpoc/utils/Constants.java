package fr.tse.projetpoc.utils;

/**
 * The project constants class
 */
public class Constants {
	
	// Roles
	final public static Long ROLE_DEVELOPER_ID = 1L;
	
	final public static Long ROLE_ADMIN_ID = 2L;
	
	final public static Long ROLE_MANAGER_ID = 3L;

	
	// Milestones
	final public static Long MILESTONE_INITIALISATION_ID = 1L;
	
	final public static Long MILESTONE_CONCEPTION_ID = 2L;
	
	final public static Long MILESTONE_REALISATION_ID = 3L;
	
	final public static Long MILESTONE_DEPLOYMENT_ID = 4L;
	
	final public static String MILESTONE_INITIALISATION_LABEL = "Initialisation";
	
	final public static String MILESTONE_CONCEPTION_LABEL = "Conception";
	
	final public static String MILESTONE_REALISATION_LABEL = "Realisation";
	
	final public static String MILESTONE_DEPLOYMENT_LABEL = "Deployment";
	
	// Cros origin
	final public static String CROSS_ORIGIN = "http://localhost:4200";
	
}
