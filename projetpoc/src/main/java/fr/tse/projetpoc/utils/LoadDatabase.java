package fr.tse.projetpoc.utils;

import java.sql.Date;
import java.time.LocalDate;
import java.util.GregorianCalendar;
import java.util.List;

import fr.tse.projetpoc.domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import fr.tse.projetpoc.dao.LogsRepository;
import fr.tse.projetpoc.dao.MilestoneRepository;
import fr.tse.projetpoc.dao.MilestoneTitleRepository;
import fr.tse.projetpoc.dao.PersonRepository;
import fr.tse.projetpoc.dao.ProjectRepository;
import fr.tse.projetpoc.dao.RoleRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * Class used to populate the database
 */
@Configuration
@Slf4j
public class LoadDatabase {
	@Autowired
	PasswordEncoder passwordEncoder;
	
	/**
	 * Populate database
	 * @param logsRepository The logs repository parameter
	 * @param milestoneRepository The milestone repository parameter
	 * @param milestoneTitleRepository The milestone title repository parameter
	 * @param personRepository The person repository parameter
	 * @param projectRepository The project repository parameter
	 * @param roleRepository The role repository parameter
	 * @return
	 */
	@Bean
	@Profile("!test")
	CommandLineRunner initDatabase(LogsRepository logsRepository,
									   MilestoneRepository milestoneRepository,
									   MilestoneTitleRepository milestoneTitleRepository,
									   PersonRepository personRepository,
									   ProjectRepository projectRepository,
									   RoleRepository roleRepository) {
		
		return args -> {

			initRole(roleRepository,false);
			
			initPerson(personRepository, roleRepository,false);

			initMilestoneTitle(milestoneTitleRepository,false);
			
			initMilestonesAndProject(milestoneRepository, projectRepository, personRepository, milestoneTitleRepository,false);
			
			initLogs(logsRepository, milestoneRepository, projectRepository, personRepository,false);
		};
	}

	/**
	 * Populate database for tests corresponding to the "test" profile 
	 * @param logsRepository The logs repository parameter
	 * @param milestoneRepository The milestone repository parameter
	 * @param milestoneTitleRepository The milestone title repository parameter
	 * @param personRepository The person repository parameter
	 * @param projectRepository The project repository parameter
	 * @param roleRepository The role repository parameter
	 * @return
	 */
	@Bean
	@Profile("test")
	CommandLineRunner initTestDatabase(LogsRepository logsRepository,
									   MilestoneRepository milestoneRepository,
									   MilestoneTitleRepository milestoneTitleRepository,
									   PersonRepository personRepository,
									   ProjectRepository projectRepository,
									   RoleRepository roleRepository) {
		
		return args -> {
			clearDataBase(roleRepository, personRepository, projectRepository,
					milestoneTitleRepository, milestoneRepository, logsRepository);

			initRole(roleRepository,true);
			
			initPerson(personRepository, roleRepository,true);

			initMilestoneTitle(milestoneTitleRepository,true);
			
			initMilestonesAndProject(milestoneRepository, projectRepository, personRepository, milestoneTitleRepository,true);
			
			initLogs(logsRepository, milestoneRepository, projectRepository, personRepository, true);
		};
	}

	/**
	 * Clear database
	 */
	public void clearDataBase(RoleRepository roleRepository,
							  PersonRepository personRepository,
							  ProjectRepository projectRepository,
							  MilestoneTitleRepository milestoneTitleRepository,
							  MilestoneRepository milestoneRepository,
							  LogsRepository logsRepository){
		log.info("DB must be clear");
		if(logsRepository.count() != 0)
			logsRepository.deleteAll();
		if(personRepository.count() != 0)
			personRepository.deleteAll();
		if(roleRepository.count() != 0)
			roleRepository.deleteAll();
		if(projectRepository.count() != 0)
			projectRepository.deleteAll();
		if(milestoneRepository.count() != 0)
			milestoneRepository.deleteAll();
		if(milestoneTitleRepository.count() != 0)
			milestoneTitleRepository.deleteAll();

		log.info("DB cleared");
	}


	/**
	 * Populate database with roles
	 * @param roleRepository The role repository parameter
	 */
	public void initRole(RoleRepository roleRepository, boolean test) {
		if(roleRepository.count() == 0 || test){
			// Create roles
			Role adminRole = new Role(Constants.ROLE_ADMIN_ID, RoleType.Admin);
			Role managerRole = new Role(Constants.ROLE_MANAGER_ID, RoleType.Manager);
			Role developerRole = new Role(Constants.ROLE_DEVELOPER_ID, RoleType.Developer);

			// Save roles
			roleRepository.save(adminRole);
			roleRepository.save(managerRole);
			roleRepository.save(developerRole);

			log.info("Create admin, manager, developer roles");
		}
	}
	
	/**
	 * Populate database with contributors
	 * @param personRepository The person repository parameter
	 * @param roleRepository The role repository parameter
	 */
	public void initPerson(PersonRepository personRepository, RoleRepository roleRepository, boolean test) {
		if(personRepository.count() == 0|| test) {
			// Create person
			Person personMerley = new Person(null, "Audric", "Merley", "gryffondor", this.passwordEncoder.encode("1234"), roleRepository.findById(Constants.ROLE_ADMIN_ID).orElse(null));
			Person personDrogo = new Person(null, "Richard", "Drogo", "acerSwift3", this.passwordEncoder.encode("jacamo"), roleRepository.findById(Constants.ROLE_DEVELOPER_ID).orElse(null));
			Person personCedric = new Person(null, "Cedric", "Gormond", "cedgor", this.passwordEncoder.encode("ilovejacamo"), roleRepository.findById(Constants.ROLE_DEVELOPER_ID).orElse(null));
			Person personBertin = new Person(null, "Tristan", "Bertin", "tristetemps", this.passwordEncoder.encode("dambenois"), roleRepository.findById(Constants.ROLE_MANAGER_ID).orElse(null));

			// Save person
			personMerley = personRepository.save(personMerley);
			personDrogo = personRepository.save(personDrogo);
			personCedric = personRepository.save(personCedric);
			personBertin = personRepository.save(personBertin);

			personMerley.addOrChangeManager(personBertin);
			personMerley = personRepository.save(personMerley);

			log.info("Create 4 person");
		}
	}
	
	/**
	 * Populate database with milestone titles
	 * @param milestoneTitleRepository The milestone repository parameter
	 */
	public void initMilestoneTitle(MilestoneTitleRepository milestoneTitleRepository, boolean test) {
		if(milestoneTitleRepository.count() == 0|| test) {
			// Create milestonetitles
			MilestoneTitle milestoneTitleInitialisation = new MilestoneTitle(Constants.MILESTONE_INITIALISATION_ID, Constants.MILESTONE_INITIALISATION_LABEL);
			MilestoneTitle milestoneTitleConception = new MilestoneTitle(Constants.MILESTONE_CONCEPTION_ID, Constants.MILESTONE_CONCEPTION_LABEL);
			MilestoneTitle milestoneTitleRealisation = new MilestoneTitle(Constants.MILESTONE_REALISATION_ID, Constants.MILESTONE_REALISATION_LABEL);
			MilestoneTitle milestoneTitleDeployment = new MilestoneTitle(Constants.MILESTONE_DEPLOYMENT_ID, Constants.MILESTONE_DEPLOYMENT_LABEL);

			// Save milestonetitle
			milestoneTitleRepository.save(milestoneTitleInitialisation);
			milestoneTitleRepository.save(milestoneTitleConception);
			milestoneTitleRepository.save(milestoneTitleRealisation);
			milestoneTitleRepository.save(milestoneTitleDeployment);

			log.info("Create initialisation, conception, realisation and deployment milestone titles");
		}
	}
	
	
	/**
	 * Populate database with milestones and projects
	 * @param milestoneRepository The milestone repository parameter
	 * @param projectRepository The project repository parameter
	 * @param personRepository The person repository parameter
	 * @param milestoneTitleRepository The milestone title repository parameter
	 */
	public void initMilestonesAndProject(MilestoneRepository milestoneRepository, ProjectRepository projectRepository, PersonRepository personRepository, MilestoneTitleRepository milestoneTitleRepository, boolean test) {
		if(projectRepository.count() == 0|| test) {
			// Get the developers list
			List<Person> personList = personRepository.findAll();

			// Create projects
			Project projectKanban = new Project(null, "Kanban");
			Project projectMetadev = new Project(null, "Metadev");
			Project projectPoc = new Project(null, "POC");

			// Create milestones
			Milestone milestone1 = new Milestone(null, "Persistence Conception", LocalDate.of(2021, 1, 11), LocalDate.of(2021, 1, 15), null, milestoneTitleRepository.findById(Constants.MILESTONE_CONCEPTION_ID).orElse(null));
			Milestone milestone2 = new Milestone(null, "Repository Conception", LocalDate.of(2021, 1, 11), LocalDate.of(2021, 1, 15), null, milestoneTitleRepository.findById(Constants.MILESTONE_CONCEPTION_ID).orElse(null));
			Milestone milestone3 = new Milestone(null, "Service Conception", LocalDate.of(2021, 1, 18), LocalDate.of(2021, 1, 22), null, milestoneTitleRepository.findById(Constants.MILESTONE_CONCEPTION_ID).orElse(null));
			Milestone milestone4 = new Milestone(null, "Realisation", LocalDate.of(2021, 1, 25), LocalDate.of(2021, 1, 29), null, milestoneTitleRepository.findById(Constants.MILESTONE_REALISATION_ID).orElse(null));
			Milestone milestone5 = new Milestone(null, "Deployment", LocalDate.of(2021, 2, 25), LocalDate.of(2021, 3, 29), null, milestoneTitleRepository.findById(Constants.MILESTONE_REALISATION_ID).orElse(null));

			// Save milestones
			milestone1 = milestoneRepository.save(milestone1);
			milestone2 = milestoneRepository.save(milestone2);
			milestone3 = milestoneRepository.save(milestone3);
			milestone4 = milestoneRepository.save(milestone4);
			milestone5 = milestoneRepository.save(milestone5);
			log.info("Create 5 milestones");

			// Save projects
			projectKanban = projectRepository.save(projectKanban);
			projectMetadev = projectRepository.save(projectMetadev);
			projectPoc = projectRepository.save(projectPoc);
			log.info("Create 3 projects");

			// Link milestones to project
			projectKanban.addMilestone(milestone1);
			projectKanban.addMilestone(milestone2);
			projectPoc.addMilestone(milestone3);
			projectMetadev.addMilestone(milestone4);
			projectMetadev.addMilestone(milestone5);

			// Link contributors to project
			projectKanban.addContributor(personRepository.findByLastName("Drogo").get(0));
			projectKanban.addContributor(personRepository.findByLastName("Gormond").get(0));
			projectPoc.addContributor(personRepository.findByLastName("Gormond").get(0));
			projectMetadev.addContributor(personRepository.findByLastName("Bertin").get(0));

			// Save the 3 projects after milestone linking
			projectKanban = projectRepository.save(projectKanban);
			projectMetadev = projectRepository.save(projectMetadev);
			projectPoc = projectRepository.save(projectPoc);

			log.info("Save 3 projects after milestone and contributors linking");
		}

	}

	/**
	 * Populate database with logs
	 * @param logsRepository The logs repository parameter
	 * @param milestoneRepository The milestone repository parameter
	 * @param projectRepository The project repository parameter
	 * @param personRepository The person repository parameter
	 */
	public void initLogs(LogsRepository logsRepository, MilestoneRepository milestoneRepository, ProjectRepository projectRepository, PersonRepository personRepository, boolean test) {
		if(logsRepository.count() == 0 || test) {
			// Get the milestones list
			List<Milestone> milestoneList = milestoneRepository.findAll();

			Logs logs1 = new Logs(null, 0.2, LocalDate.of(2021, 1, 12), "Create classes", milestoneList.get(0), personRepository.findByLastName("Drogo").get(0));
			Logs logs2 = new Logs(null, 0.6, LocalDate.of(2021, 1, 15), "Create repositories", milestoneList.get(1), personRepository.findByLastName("Gormond").get(0));
			Logs logs3 = new Logs(null, 0.6, LocalDate.of(2021, 1, 15), "Create services", milestoneList.get(2), personRepository.findByLastName("Gormond").get(0));
			Logs logs4 = new Logs(null, 0.8, LocalDate.of(2021, 1, 19), "Delete comments", milestoneList.get(0), personRepository.findByLastName("Drogo").get(0));
			Logs logs5 = new Logs(null, 0.9, LocalDate.of(2021, 1, 11), "Add comments", milestoneList.get(1), personRepository.findByLastName("Gormond").get(0));
			Logs logs6 = new Logs(null, 1.0, LocalDate.of(2021, 1, 1), "Change name", milestoneList.get(3), personRepository.findByLastName("Bertin").get(0));
			Logs logs7 = new Logs(null, 0.1, LocalDate.of(2021, 1, 25), "Delete tests", milestoneList.get(3), personRepository.findByLastName("Bertin").get(0));
			Logs logs8 = new Logs(null, 0.1, LocalDate.of(2021, 1, 25), "Log", milestoneList.get(4), personRepository.findByLastName("Bertin").get(0));

			logs1 = logsRepository.save(logs1);
			logs2 = logsRepository.save(logs2);
			logs3 = logsRepository.save(logs3);
			logs4 = logsRepository.save(logs4);
			logs5 = logsRepository.save(logs5);
			logs6 = logsRepository.save(logs6);
			logs7 = logsRepository.save(logs7);
			logs8 = logsRepository.save(logs8);

			log.info("Create 6 logs");
		}
	}
	
	
}
