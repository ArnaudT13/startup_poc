package fr.tse.projetpoc.controller;

import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonTranslator {
    public static String asJsonString(final Object object) {
        try {
            return new ObjectMapper().writeValueAsString(object);
        } catch (Exception e){
            throw new RuntimeException(e);
        }
    }
}
