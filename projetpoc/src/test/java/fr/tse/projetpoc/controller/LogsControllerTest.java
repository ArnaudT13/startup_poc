package fr.tse.projetpoc.controller;

import fr.tse.projetpoc.dao.RoleRepository;
import fr.tse.projetpoc.domain.*;
import fr.tse.projetpoc.service.LogsService;
import fr.tse.projetpoc.service.PersonService;
import fr.tse.projetpoc.service.ProjectService;
import org.hamcrest.collection.IsCollectionWithSize;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
@AutoConfigureMockMvc
@ActiveProfiles(profiles = "test")
public class LogsControllerTest extends ControllerTest {
    @Autowired
    private LogsService logsService;
    @Autowired
    private PersonService personService;
    @Autowired
    private WebApplicationContext context;
    @Autowired
    private ProjectService projectService;

    @BeforeEach
    public void configureSession() throws Exception {
        this.mvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(SecurityMockMvcConfigurers.springSecurity())
                .build();
    }

    @WithMockUser(username = "gryffondor", password = "1324", authorities = {"Admin"})
    @Test
    public void findAllLogsTest() throws Exception {
        List<Logs> logsList = this.logsService.findAllLogs();
        ResultActions resultAction = mvc.perform(get("/logs")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status() .isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", new IsCollectionWithSize<Logs>(is(logsList.size()))))
                .andExpect(jsonPath("$[0].id", is(logsList.get(0).getId().intValue())))
                .andExpect(jsonPath("$[1].id", is(logsList.get(1).getId().intValue()))).andDo(print());
    }

    @WithMockUser(username = "gryffondor", password = "1324", authorities = {"Admin"})
    @Test
    public void findLogsByIdTest() throws Exception {
        Logs logs = this.logsService.findAllLogs().get(2);
        ResultActions resultAction = mvc.perform(get("/logs/"+logs.getId().toString())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status() .isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("id", is(logs.getId().intValue())))
                .andExpect(jsonPath("description", is(logs.getDescription()))).andDo(print());
    }

    @WithMockUser(username = "gryffondor", password = "1324", authorities = {"Admin"})
    @Test
    public void findLogsByPersonTest() throws Exception {
        Person person = this.personService.findPersonsByFirstName("Cedric").get(0);
        ResultActions resultAction = mvc.perform(get("/logs/person/"+person.getId())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status() .isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", new IsCollectionWithSize<Logs>(is(person.getLogs().size()))))
                .andExpect(jsonPath("$[0].id", is(person.getLogs().get(0).getId().intValue())))
                .andExpect(jsonPath("$[1].id", is(person.getLogs().get(1).getId().intValue()))).andDo(print());
    }
    @WithMockUser(username = "gryffondor", password = "1324", authorities = {"Admin"})
    @Test
    public void findProjectsByLogsTest() throws Exception {
        Logs logs = this.logsService.findAllLogs().get(0);
        Project expectedProject = this.projectService.findProjectById(logs.getMilestone().getProject().getId());
        ResultActions resultAction = mvc.perform(get("/logs/"+logs.getId()+"/project")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status() .isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id", is(expectedProject.getId().intValue()))).andDo(print());
    }

    @WithMockUser(username = "gryffondor", password = "1324", authorities = {"Admin"})
    @Test
    public void findLogsByProjectAndUserTest() throws Exception {
        Logs logs = this.logsService.findAllLogs().get(0);
        Long expectedUserId = logs.getPerson().getId();
        Long expectedProjectId = logs.getMilestone().getProject().getId();
        ResultActions resultAction = mvc.perform(get("/logs/projects/"+expectedProjectId.toString()+"/person/"+expectedUserId.toString())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status() .isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].id", is(logs.getId().intValue()))).andDo(print());
    }

    @WithMockUser(username = "gryffondor", password = "1324", authorities = {"Admin"})
    @Test
    public void findLogsByProjectTest() throws Exception {
        List<Milestone> mils = this.projectService.findAllProject().get(0).getMilestones();
        List<Logs> logsProject = new ArrayList<>();
        for(Milestone mil : mils){
            if(mil.getLogs() != null && mil.getLogs().size() > 0){
                logsProject.addAll(mil.getLogs());
            }
        }
        ResultActions resultAction = mvc.perform(get("/logs/projects/"+this.projectService.findAllProject().get(0).getId().toString())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status() .isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$",new IsCollectionWithSize<Logs>(is(logsProject.size()))))
                .andExpect(jsonPath("$[0].id",is(logsProject.get(0).getId().intValue())));
    }

    @WithMockUser(username = "gryffondor", password = "1324", authorities = {"Admin"})
    @Test
    public void findMilestoneByLogsTest() throws Exception {
        Long milestoneId = this.logsService.findAllLogs().get(0).getMilestone().getId();
        ResultActions resultAction = mvc.perform(get("/logs/"+this.logsService.findAllLogs().get(0).getId()+"/milestone")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status() .isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("id", is(milestoneId.intValue()))).andDo(print());
    }
}
