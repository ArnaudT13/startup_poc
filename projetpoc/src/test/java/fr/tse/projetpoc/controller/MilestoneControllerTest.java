package fr.tse.projetpoc.controller;

import fr.tse.projetpoc.domain.Milestone;
import fr.tse.projetpoc.domain.MilestoneTitle;
import fr.tse.projetpoc.domain.Project;
import fr.tse.projetpoc.service.MilestoneService;
import fr.tse.projetpoc.service.MilestoneTitleService;
import fr.tse.projetpoc.service.ProjectService;
import org.hamcrest.collection.IsCollectionWithSize;
import org.jfree.data.time.Millisecond;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
@AutoConfigureMockMvc
@ActiveProfiles(profiles = "test")
public class MilestoneControllerTest extends ControllerTest {
    @Autowired
    private WebApplicationContext context;
    @Autowired
    private MilestoneService milestoneService;
    @Autowired
    private ProjectService projectService;
    @Autowired
    private MilestoneTitleService milestoneTitleService;
    @BeforeEach
    public void configureSession() throws Exception {
        this.mvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(SecurityMockMvcConfigurers.springSecurity())
                .build();
    }

    @WithMockUser(username = "gryffondor", password = "1324", authorities = {"Admin"})
    @Test
    public void getAllMilestonesTest() throws Exception {
        ResultActions resultAction = mvc.perform(get("/milestones")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status() .isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", new IsCollectionWithSize<Milestone>(is(this.milestoneService.findAllMilestones().size()))));
    }

    @WithMockUser(username = "gryffondor", password = "1324", authorities = {"Admin"})
    @Test
    public void getMilestoneByIdTest() throws Exception {
        Milestone milestone = this.milestoneService.findAllMilestones().get(0);
        ResultActions resultAction = mvc.perform(get("/milestones/"+milestone.getId())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status() .isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("id", is(milestone.getId().intValue())))
                .andExpect(jsonPath("description", is(milestone.getDescription())));
    }
    @WithMockUser(username = "gryffondor", password = "1324", authorities = {"Admin"})
    @Test
    public void getMilestonesByProjectTest() throws Exception {
        Project project = this.projectService.findAllProject().get(0);
        List<Milestone> expectedMilestones = project.getMilestones();
        ResultActions resultAction = mvc.perform(get("/milestones/projects/"+project.getId())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status() .isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", new IsCollectionWithSize<Milestone>(is(expectedMilestones.size()))))
                .andExpect(jsonPath("$[0].id", is(expectedMilestones.get(0).getId().intValue())))
                .andExpect(jsonPath("$[0].description", is(expectedMilestones.get(0).getDescription())));
    }

    @WithMockUser(username = "gryffondor", password = "1324", authorities = {"Admin"})
    @Test
    public void getProjectByMillestoneTest() throws Exception {
        Milestone milestone = this.milestoneService.findAllMilestones().get(0);
        Project expectedProject = milestone.getProject();
        ResultActions resultAction = mvc.perform(get("/milestones/"+milestone.getId()+"/project")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status() .isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("id", is(expectedProject.getId().intValue())))
                .andExpect(jsonPath("name", is(expectedProject.getName())));
    }

    @WithMockUser(username = "gryffondor", password = "1324", authorities = {"Admin"})
    @Test
    public void addMillestoneTest() throws Exception {
        int milestonesSize = this.milestoneService.findAllMilestones().size();
        int milestonesProjectSize = this.projectService.findProjectById(1L).getMilestones().size();
        Milestone milestone = new Milestone();
        milestone.setDescription("milestone descriptionnnnn");
        milestone.setMilestoneTitle(this.milestoneTitleService.findAllMilestonesTitle().get(0));
        ResultActions resultAction = mvc.perform(post("/project/"+1+"/milestones/add")
                .contentType(MediaType.APPLICATION_JSON)
                .content(JsonTranslator.asJsonString(milestone)))
                .andExpect(status() .isCreated())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON));
        Assertions.assertEquals(milestonesSize+1,this.milestoneService.findAllMilestones().size());
        Assertions.assertEquals(milestonesProjectSize+1,this.projectService.findProjectById(1L).getMilestones().size());
        boolean findDescr = false;
        for(Milestone actualMilestone : this.milestoneService.findAllMilestones()){
            findDescr = findDescr || (actualMilestone.getDescription().equals(milestone.getDescription()));
            if (findDescr){
                break;
            }
        }
        Assertions.assertTrue(findDescr);
    }







}
