package fr.tse.projetpoc.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.hamcrest.collection.IsCollectionWithSize;
import org.junit.jupiter.api.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import fr.tse.projetpoc.domain.MilestoneTitle;
import fr.tse.projetpoc.service.MilestoneTitleService;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Locale;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
@AutoConfigureMockMvc
@ActiveProfiles(profiles = "test")
public class MilestoneTitleControllerTest extends ControllerTest {
    @Autowired
    private MilestoneTitleService milestoneTitleService;
    @Autowired
    private WebApplicationContext context;

    @BeforeEach
    public void configureSession() throws Exception {
        this.mvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(SecurityMockMvcConfigurers.springSecurity())
                .build();
    }
    @AfterEach
    public void removeSession()throws Exception{
        //this.logout();
    }
    @WithMockUser(username = "gryffondor", password = "1324", authorities = {"Admin"})
    @Test
    public void getAllMilestoneTitlesTest() throws Exception{
        MilestoneTitle milestoneTitle1 = this.milestoneTitleService.findAllMilestonesTitle().get(0);
        MilestoneTitle milestoneTitle2 = this.milestoneTitleService.findAllMilestonesTitle().get(1);
        ResultActions resultAction = mvc.perform(get("/milestones/titles")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status() .isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", new IsCollectionWithSize<MilestoneTitle>(is(this.milestoneTitleService.findAllMilestonesTitle().size()))))
			    .andExpect(jsonPath("$[0].id", is(milestoneTitle1.getId().intValue())))
			    .andExpect(jsonPath("$[0].title", is(milestoneTitle1.getTitle())))
			    .andExpect(jsonPath("$[1].id", is(milestoneTitle2.getId().intValue())))
			    .andExpect(jsonPath("$[1].title", is(milestoneTitle2.getTitle())));
    }

    @WithMockUser(username = "gryffondor", password = "1324", authorities = {"Admin"})
    @Test
    public void getMilestoneTitleByIdTest() throws Exception{
        MilestoneTitle milestoneTitle = this.milestoneTitleService.findAllMilestonesTitle().get(0);
        ResultActions resultAction = mvc.perform(get("/milestones/titles/"+milestoneTitle.getId())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status() .isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("id", is(milestoneTitle.getId().intValue())))
                .andExpect(jsonPath("title", is(milestoneTitle.getTitle())));
    }

    @WithMockUser(username = "gryffondor", password = "1324", authorities = {"Admin"})
    @Test
    public void addMilestoneTitleTest() throws Exception{
        String expectedName = "TEST TEST";
        MilestoneTitle milestoneTitle = new MilestoneTitle(295L,expectedName);

        ResultActions resultAction = mvc.perform(MockMvcRequestBuilders
                .post("/milestones/titles/add")
                .content(JsonTranslator.asJsonString(milestoneTitle))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andDo(print());

        boolean found = false;
        for(MilestoneTitle actualMilestone: this.milestoneTitleService.findAllMilestonesTitle()){
            found = actualMilestone.getTitle().equals(expectedName);
            if(found)
                break;
        }
        Assertions.assertTrue(found,"The milestone title wasn't added");
    }

    @WithMockUser(username = "gryffondor", password = "1324", authorities = {"Admin"})
    @Test
    public void addMilestoneTitleTwoTimesTest() throws Exception{
        String expectedName = "TEST TEST MULTIPLE";
        MilestoneTitle milestoneTitle1 = new MilestoneTitle(152L,expectedName);
        MilestoneTitle milestoneTitle2 = new MilestoneTitle(153L,expectedName);
        ResultActions resultAction = mvc.perform(MockMvcRequestBuilders
                .post("/milestones/titles/add")
                .content(JsonTranslator.asJsonString(milestoneTitle1))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andDo(print());

        resultAction = mvc.perform(MockMvcRequestBuilders
                .post("/milestones/titles/add")
                .content(JsonTranslator.asJsonString(milestoneTitle2))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andDo(print());

        boolean found = false;

    }
}
