package fr.tse.projetpoc.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import fr.tse.projetpoc.utils.RoleType;
import org.hamcrest.collection.IsCollectionWithSize;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.internal.matchers.Null;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import fr.tse.projetpoc.dao.RoleRepository;
import fr.tse.projetpoc.domain.Person;
import fr.tse.projetpoc.domain.Role;
import fr.tse.projetpoc.service.PersonService;
import fr.tse.projetpoc.utils.Constants;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@AutoConfigureMockMvc
@ActiveProfiles(profiles = "test")
public class PersonControllerTest extends ControllerTest {
    @Autowired
    private PersonService personService;
    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private WebApplicationContext context;

    @BeforeEach
    public void configureSession() throws Exception {
        this.mvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(SecurityMockMvcConfigurers.springSecurity())
                .build();
    }

    @WithMockUser(username = "gryffondor", password = "1324", authorities = {"Admin"})
    @Test
    public void getAllPersonsTest() throws Exception{
        Person person1 = this.personService.findAllPerson().get(0);
        Person person2 = this.personService.findAllPerson().get(1);
        ResultActions resultAction = mvc.perform(get("/persons")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status() .isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
			    .andExpect(jsonPath("$[0].id", is(person1.getId().intValue())))
			    .andExpect(jsonPath("$[0].firstName", is(person1.getFirstName())))
			    .andExpect(jsonPath("$[1].id", is(person2.getId().intValue())))
			    .andExpect(jsonPath("$[1].firstName", is(person2.getFirstName())));
    }

    @WithMockUser(username = "gryffondor", password = "1324", authorities = {"Admin"})
    @Test
    public void getPersonByIdTest() throws Exception{
        Person person = this.personService.findAllPerson().get(0);
        ResultActions resultAction = mvc.perform(get("/person/"+person.getId())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status() .isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("id", is(person.getId().intValue())));
    }

    @WithMockUser(username = "gryffondor", password = "1324", authorities = {"Admin"})
    @Test
    public void getPersonsByNameTest() throws Exception{
        Person person = this.personService.findAllPerson().get(0);
        // firstname
        ResultActions resultAction = mvc.perform(get("/persons/firstName?firstName="+person.getFirstName())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status() .isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].id", is(person.getId().intValue())));

        // lastname
        Person person2 = this.personService.findAllPerson().get(1);
        resultAction = mvc.perform(get("/persons/lastName?lastName="+person2.getLastName())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status() .isOk())
                .andDo(print())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].id", is(person2.getId().intValue())));

        resultAction = mvc.perform(get("/persons/lastName?lastName=YTDHUCNSQdsf54")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status() .isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", new IsCollectionWithSize<String>(is(0))));


        // userName
        Person person3 = this.personService.findAllPerson().get(2);
        resultAction = mvc.perform(get("/person/username?username="+person3.getLogin())
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status() .isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id", is(person3.getId().intValue())));
    }

    private String createPerson(String firstName,String lastName,String login,String password,int roleId,String roleLabel){
        // create JSON
        Map<String,Object> jsonPerson = new HashMap<>();
        Map<String,Object> jsonRole = new HashMap<>();
        List<Object> projects = new ArrayList<>();
        jsonRole.put("id",roleId);
        jsonRole.put("label",roleLabel);
        jsonPerson.put("firstName",firstName);
        jsonPerson.put("lastName",lastName);
        jsonPerson.put("login",login);
        jsonPerson.put("password",password);
        jsonPerson.put("role",jsonRole);
        jsonPerson.put("projects",projects);
        jsonPerson.put("manager",null);
        return JsonTranslator.asJsonString(jsonPerson);
    }
    @WithMockUser(username = "gryffondor", password = "1324", authorities = {"Admin"})
    @Test
    public void addPersonTest() throws Exception{
        String personStr = this.createPerson("Bjarne","Stroustrup","C++","C++betterThanJava;)1234",2,"Admin");
        ResultActions resultAction = mvc.perform(MockMvcRequestBuilders
                .post("/person/add")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(personStr))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON));

        Assertions.assertTrue(!this.personService.findPersonsByFirstName("Bjarne").isEmpty());
        Person actualPerson = this.personService.findPersonsByFirstName("Bjarne").get(0);
        Assertions.assertTrue(actualPerson.getFirstName().equals("Bjarne"));
        Assertions.assertTrue(actualPerson.getLastName().equals("Stroustrup"));
        Assertions.assertTrue(actualPerson.getLogin().equals("C++"));

        String person2Str = this.createPerson("James","Gosling","C++","JavabetterThanC++;)1234",2,"Admin");
        ResultActions resultAction2 = mvc.perform(MockMvcRequestBuilders
                .post("/person/add")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(person2Str))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON));
    }

    @WithMockUser(username = "gryffondor", password = "1324", authorities = {"Admin"})
    @Test
    public void modifyPersonTest() throws Exception{
        Person person = this.personService.findPersonsByFirstName("Audric").get(0);
        Role role = this.roleRepository.findById(Constants.ROLE_DEVELOPER_ID).orElse(null);
        Assertions.assertNotNull(role);
        mvc.perform(MockMvcRequestBuilders
                .patch("/person/"+person.getId().toString()+"/role/"+role.getId().toString())
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
        Person actualPerson = this.personService.findPersonById(person.getId());
        Assertions.assertTrue(actualPerson.getRole().getLabel().equals(role.getLabel()));
        Assertions.assertFalse(actualPerson.getRole().getLabel().equals(person.getRole().getLabel()));
    }


    @WithMockUser(username = "gryffondor", password = "1324", authorities = {"Admin"})
    @Test
    public void removePersonTest() throws Exception {
        Person person = new Person(null,"Sun","Microsystème","Oracle","JavaisBetterThanC++",this.roleRepository.findById(Constants.ROLE_ADMIN_ID).orElse(null));
        person = this.personService.addPerson(person);
        mvc.perform(MockMvcRequestBuilders
                .patch("/person/"+person.getId().toString()+"/remove")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
        Assertions.assertTrue(this.personService.findPersonById(person.getId()) == null, "The person wasn't removed.");

    }


    @WithMockUser(username = "gryffondor", password = "1324", authorities = {"Admin"})
    @Test
    public void findAllPersonsForAManagerTest() throws Exception{
        Person developerMerley = this.personService.findPersonsByLastName("Merley").get(0);
        Person managerBertin = this.personService.findPersonsByLastName("Bertin").get(0);
        ResultActions resultAction = mvc.perform(get("/persons/" + managerBertin.getId() + "/developers")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status() .isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
			    .andExpect(jsonPath("$[0].id", is(developerMerley.getId().intValue())))
			    .andExpect(jsonPath("$[0].firstName", is(developerMerley.getFirstName())));
    }


    @WithMockUser(username = "gryffondor", password = "1324", authorities = {"Admin"})
    @Test
    public void findAllProjectForPersonTest() throws Exception {
    	Person developerGormond = this.personService.findPersonsByLastName("Gormond").get(0);
        ResultActions resultAction = mvc.perform(get("/persons/" + developerGormond.getId() + "/projects")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status() .isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
			    .andExpect(jsonPath("$.length()", is(2)));
    }

    @WithMockUser(username = "gryffondor", password = "1324", authorities = {"Admin"})
    @Test
    public void changeUserManager() throws Exception{
        Person manager = this.personService.findPersonsByFirstName("Tristan").get(0);
        Person dev = this.personService.findPersonsByFirstName("Cedric").get(0);
        ResultActions resultAction = mvc.perform(patch("/person/" + dev.getId() + "/manager/"+manager.getId())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status() .isOk()).andDo(print());
        Person devActual = this.personService.findPersonById(dev.getId());
        Assertions.assertNotEquals(dev.getManager(),devActual.getManager());
        Assertions.assertEquals(manager.getId(),devActual.getManager().getId());

    }

    @WithMockUser(username = "gryffondor", password = "1324", authorities = {"Admin"})
    @Test
    public void removeUserManager()throws  Exception{
        Person devMerley = this.personService.findPersonsByFirstName("Audric").get(0);
        Person manager = this.personService.findPersonById(devMerley.getManager().getId());
        ResultActions resultAction = mvc.perform(patch("/person/" + devMerley.getId() + "/remove_manager")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status() .isOk()).andDo(print());
        Person devActual = this.personService.findPersonById(devMerley.getId());
        Person managerActual = this.personService.findPersonById(manager.getId());
        Assertions.assertNotNull(managerActual);
        Assertions.assertNull(devActual.getManager());
    }
}
