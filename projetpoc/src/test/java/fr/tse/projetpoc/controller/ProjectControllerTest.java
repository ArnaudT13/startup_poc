package fr.tse.projetpoc.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import org.hamcrest.collection.IsCollectionWithSize;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.ResultActions;

import fr.tse.projetpoc.domain.Milestone;
import fr.tse.projetpoc.domain.Person;
import fr.tse.projetpoc.domain.Project;
import fr.tse.projetpoc.service.MilestoneService;
import fr.tse.projetpoc.service.PersonService;
import fr.tse.projetpoc.service.ProjectService;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
@AutoConfigureMockMvc
@ActiveProfiles(profiles = "test")
public class ProjectControllerTest extends ControllerTest {

    @Autowired
    ProjectService projectService;
    @Autowired
    PersonService personService;
    @Autowired
    MilestoneService milestoneService;

    @Autowired
    private WebApplicationContext context;

    @BeforeEach
    public void configureSession() throws Exception {
        this.mvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(SecurityMockMvcConfigurers.springSecurity())
                .build();
    }
    @WithMockUser(username = "gryffondor", password = "1324", authorities = {"Admin"})
    @Test
    public void findAllProjectTest() throws Exception{
        Project project1 = this.projectService.findAllProject().get(0);
        Project project2 = this.projectService.findAllProject().get(1);
        ResultActions resultAction = mvc.perform(get("/projects")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status() .isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$",new IsCollectionWithSize<Project>(is(this.projectService.findAllProject().size()))))
                .andExpect(jsonPath("$[0].id", is(project1.getId().intValue())))
                .andExpect(jsonPath("$[0].name", is(project1.getName())))
                .andExpect(jsonPath("$[1].id", is(project2.getId().intValue())))
                .andExpect(jsonPath("$[1].name", is(project2.getName())));
    }

    @WithMockUser(username = "gryffondor", password = "1324", authorities = {"Admin"})
    @Test
    public void findProjectsByNameTest() throws Exception{
        List<Project> projects = this.projectService.findProjectsByName("Kanban");
        Assertions.assertNotNull(projects);
        Assertions.assertFalse(projects.isEmpty());

        ResultActions resultAction = mvc.perform(get("/projects/name?name="+projects.get(0).getName())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status() .isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$",new IsCollectionWithSize<Project>(is(projects.size()))))
                .andExpect(jsonPath("$[0].id", is(projects.get(0).getId().intValue())))
                .andExpect(jsonPath("$[0].name", is(projects.get(0).getName())));
    }

    @WithMockUser(username = "gryffondor", password = "1324", authorities = {"Admin"})
    @Test
    public void findProjectByIdTest() throws Exception{
        Project project = this.projectService.findAllProject().get(0);
        ResultActions resultAction = mvc.perform(get("/projects/"+project.getId())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status() .isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("id", is(project.getId().intValue())));
    }

    @WithMockUser(username = "gryffondor", password = "1324", authorities = {"Admin"})
    @Test
    public void addProjectTest() throws Exception{
        Project project = new Project();
        project.setName("test");
        ResultActions resultAction = mvc.perform(post("/project/add")
                .content(JsonTranslator.asJsonString(project))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON));

        Assertions.assertFalse(this.projectService.findProjectsByName("test").isEmpty());
    }

    @WithMockUser(username = "gryffondor", password = "1324", authorities = {"Admin"})
    @Test
    public void removeProjectTest() throws Exception{
        Project project = this.projectService.findProjectsByName("test").get(0);
        Assertions.assertNotNull(project);
        ResultActions resultAction = mvc.perform(post("/project/remove")
                .content(JsonTranslator.asJsonString(project))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON));
        Assertions.assertTrue(this.projectService.findProjectsByName("test").isEmpty());

    }

    @WithMockUser(username = "gryffondor", password = "1324", authorities = {"Admin"})
    @Test
    public void addMilestoneTest() throws Exception{
        //@PatchMapping(value = "/projects/{id}/add_milestone", produces = "application/json")
        Long id = this.projectService.findAllProject().get(0).getId();
        Milestone milestone = new Milestone();
        milestone.setDescription("Test test");
        // problem if milestione doesn't exist in table
        ResultActions resultAction = mvc.perform(patch("/projects/"+id.toString()+"/add_milestone")
                .content(JsonTranslator.asJsonString(milestone))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON));

        Milestone goodMilestione = this.projectService.findProjectById(id).getMilestones()
                .stream()
                .filter(s -> s.getDescription().equals("Test test"))
                .findFirst().orElse(null);
        Assertions.assertNotNull(goodMilestione);
    }

    @WithMockUser(username = "gryffondor", password = "1324", authorities = {"Admin"})
    @Test
    public void deleteMilestoneTest() throws Exception{
        //@PatchMapping(value = "/projects/{id}/delete_milestone/{id}", produces = "application/json")
        int milestonesSize = this.projectService.findAllProject().get(0).getMilestones().size();
        Assertions.assertTrue(milestonesSize > 0);
        Long milestoneId = this.projectService.findAllProject().get(0).getMilestones().get(0).getId();
        ResultActions resultAction = mvc.perform(patch("/project/"+this.projectService.findAllProject().get(0)+"/delete_milestone/"+milestoneId)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON));

        Assertions.assertEquals(milestonesSize-1,this.projectService.findAllProject().get(0).getMilestones().size());
    }

    @WithMockUser(username = "gryffondor", password = "1324", authorities = {"Admin"})
    @Test
    public void addContributorTest() throws Exception{
        //@PatchMapping(value = "/projects/{id}/add_contributor", produces = "application/json")
        Person newContributor = this.personService.findPersonsByFirstName("Tristan").get(0);
        ResultActions resultAction = mvc.perform(patch("/project/"+this.projectService.findAllProject().get(0)+"/add_contributor/"+newContributor.getId().toString())
                .content(JsonTranslator.asJsonString(newContributor))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        List<Person> actualContributor = this.projectService.findAllProject().get(0).getContributors();
        boolean added = false;
        for(Person user : actualContributor){
            if(user.getId() == newContributor.getId()){
                added = true;
                break;
            }
        }
        Assertions.assertTrue(added);
    }

    @WithMockUser(username = "gryffondor", password = "1324", authorities = {"Admin"})
    @Test
    public void deleteContributorTest() throws Exception{
        //@PatchMapping(value = "/projects/{id}/delete_contributor/{id}", produces = "application/json")
        int contributorSize = this.projectService.findAllProject().get(0).getContributors().size();
        Assertions.assertTrue(contributorSize > 0);
        Long contributorId = this.projectService.findAllProject().get(0).getContributors().get(0).getId();
        ResultActions resultAction = mvc.perform(patch("/project/"+this.projectService.findAllProject().get(0)+"/delete_contributor/"+contributorId)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON));

        Assertions.assertEquals(contributorSize-1,this.projectService.findAllProject().get(0).getContributors().size());
    }
/*
    @Test
    public void addLogs() throws Exception{
        //@PatchMapping(value = "/projects/{id_project}/persons/{id_person}/add_logs", produces = "application/json")
        Person person = new Person();
        person.setFirstName("Test add logs");
        person = this.personService.addPerson(person);

        Logs logs = new Logs();
        logs.setDescription("Test logs logs");

        int logsSize = this.projectService.findAllProject().get(0).getLogs().size();

        ResultActions resultAction = mvc.perform(patch("/projects/"+this.projectService.findAllProject().get(0).getId()
                +"/persons/"+person.getId()+"/add_logs/")
                .content(JsonTranslator.asJsonString(logs))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON));
        Assertions.assertEquals(logsSize+1,this.projectService.findAllProject().get(0).getLogs().size());

        Logs actualLogs = this.projectService.findAllProject().get(0).getLogs()
                .stream().filter(l -> l.getDescription().equals("Test logs logs")).findFirst().orElse(null);
        Assertions.assertNotNull(actualLogs);
        Assertions.assertTrue(actualLogs.getPerson().getFirstName().equals("Test add logs"));
    }*/
}
