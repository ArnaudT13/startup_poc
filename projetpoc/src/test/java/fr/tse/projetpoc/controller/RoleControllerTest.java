package fr.tse.projetpoc.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.hamcrest.collection.IsCollectionWithSize;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.ResultActions;

import fr.tse.projetpoc.domain.Project;
import fr.tse.projetpoc.domain.Role;
import fr.tse.projetpoc.service.RoleService;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
@AutoConfigureMockMvc
@ActiveProfiles(profiles = "test")
public class RoleControllerTest extends ControllerTest {
    @Autowired
    private RoleService roleService;
    @Autowired
    private WebApplicationContext context;

    @BeforeEach
    public void configureSession() throws Exception {
        this.mvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(SecurityMockMvcConfigurers.springSecurity())
                .build();
    }
    @WithMockUser(username = "gryffondor", password = "1324", authorities = {"Admin"})
    @Test
    public void findAllRolesTest() throws Exception{
        Role role1 = this.roleService.findAllRole().get(0);
        ResultActions resultAction = mvc.perform(get("/roles")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status() .isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$",new IsCollectionWithSize<Project>(is(this.roleService.findAllRole().size()))))
                .andExpect(jsonPath("$[0].id", is(this.roleService.findAllRole().get(0).getId().intValue())))
                .andExpect(jsonPath("$[0].label", is(this.roleService.findAllRole().get(0).getLabel().toString())))
                .andExpect(jsonPath("$[1].id", is(this.roleService.findAllRole().get(1).getId().intValue())))
                .andExpect(jsonPath("$[1].label", is(this.roleService.findAllRole().get(1).getLabel().toString())));
    }
    @WithMockUser(username = "gryffondor", password = "1324", authorities = {"Admin"})
    @Test
    public void findRoleByIdTest() throws Exception{
        Role role = this.roleService.findAllRole().get(0);
        ResultActions resultAction = mvc.perform(get("/roles/"+role.getId())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status() .isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("id", is(role.getId().intValue())))
                .andExpect(jsonPath("label", is(role.getLabel().toString())));
    }
}

