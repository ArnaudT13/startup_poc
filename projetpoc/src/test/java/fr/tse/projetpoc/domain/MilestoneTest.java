package fr.tse.projetpoc.domain;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDate;
import java.util.Date;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * Class used to test milestone entity
 */
class MilestoneTest {
	
	private Milestone milestone;
	
	private Logs logs;
	
	@BeforeEach
	public void initProject() {
		milestone = new Milestone(null, "Repository Conception", LocalDate.of(2021,1,11), LocalDate.of(2021,1,15), null, new MilestoneTitle(null, "Milesone 1"));
		logs =  new Logs(null, 0.2, LocalDate.of(2021,1,12), "Test", null, null);
	}

	@Test
	void addLogsTest() {
		milestone.addLogs(logs);
		assertEquals(1, milestone.getLogs().size());
	}

}
