package fr.tse.projetpoc.domain;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDate;
import java.util.Date;

import fr.tse.projetpoc.utils.RoleType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * Class use to test project entity
 */
public class ProjectTest {
	
	private Project project;
	
	private Person person;
	
	private Milestone milestone;
	
	private Logs logs;
	
	@BeforeEach
	public void initProject() {
		project = new Project(null, "Kanban");
		person = new Person(null, "Audric", "Merley", "gryffondor", "1234", new Role(null, RoleType.Manager));
		milestone = new Milestone(null, "Repository Conception", LocalDate.of(2021,1,11), LocalDate.of(2021,1,15), null, new MilestoneTitle(null, "Milesone 1"));
		logs =  new Logs(null, 0.2, LocalDate.of(2021,1,12), "Test", null, null);
	}

	@Test
	public void addMilestoneTest() {
		project.addMilestone(milestone);
		assertEquals(1, project.getMilestones().size());
		assertEquals(milestone.getId(), project.getMilestones().get(0).getId());
	}
	
	@Test
	public void deleteMilestoneTest() {
		project.deleteMilestone(milestone);
		assertEquals(0, project.getContributors().size());
	}
	
	@Test
	public void addContributorTest() {
		project.addContributor(person);
		assertEquals(1, project.getContributors().size());
		assertEquals(person.getId(), project.getContributors().get(0).getId());
	}
	
	@Test
	public void deleteContributorTest() {
		project.deleteContributor(person);
		assertEquals(0, project.getContributors().size());
	}
	/*
	@Test
	public void addLogsTest() {
		project.addLogs(logs);
		assertEquals(1, project.getLogs().size());
	}*/
}
