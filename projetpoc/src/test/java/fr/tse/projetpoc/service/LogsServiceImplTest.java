package fr.tse.projetpoc.service;


import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;

import fr.tse.projetpoc.domain.Logs;
import fr.tse.projetpoc.domain.Person;
import fr.tse.projetpoc.domain.Project;
import javassist.tools.rmi.ObjectNotFoundException;

@SpringBootTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
@ActiveProfiles(profiles = "test")
public class LogsServiceImplTest {
	
    @Autowired
    LogsService logsService;

    @Autowired
    PersonService personService;
    
    @Autowired
    ProjectService projectService;
    
    @Autowired
    MilestoneService milestoneService;
    
    
    @Test
    @Order(1)
    public void findAllLogsTest() {
    	List<Logs> logsList = logsService.findAllLogs();
    	assertEquals(8, logsList.size());
    }
    
    @Test
    public void findLogsByIdTest() {
    	Logs logsExpected = logsService.findAllLogs().get(0);
    	Logs logsActual = logsService.findLogsById(logsExpected.getId());
    	assertEquals(logsExpected.getId(), logsActual.getId());
    	assertEquals(logsExpected.getDescription(), logsActual.getDescription());
    }
    
    @Test
    public void findLogsByProjectTest() {
    	Project project = projectService.findProjectsByName("Kanban").get(0);
    	List<Logs> logsList = logsService.findLogsByProject(project);
    	assertEquals(4, logsList.size()); // Check logs list size
    }
    
    @Test
    public void findLogsByPersonTest() {
    	Person person = personService.findPersonsByLastName("Drogo").get(0);
    	List<Logs> logsList = logsService.findLogsByPerson(person);
    	assertEquals(2, logsList.size()); // Check logs list size
    	
    	// Create list of logs description stored in database
    	List<String> logsDescriptionList = Arrays.asList(new String[]{"Create classes", "Delete comments"});
    	assertEquals(true, logsDescriptionList.contains(logsList.get(0).getDescription()));
    	assertEquals(true, logsDescriptionList.contains(logsList.get(1).getDescription()));
    }
    
    @Test
    public void findLogsByPersonAndProjectTest() {
    	Project project = projectService.findProjectsByName("Kanban").get(0);
    	Person person = personService.findPersonsByLastName("Gormond").get(0);
    	List<Logs> logsList = logsService.findLogsByPersonAndProject(person, project);
    	assertEquals(2, logsList.size()); // Check logs list size
    }
    
    @Test
    public void removeLogsTest() throws ObjectNotFoundException {
    	int initialLogsListSize = logsService.findAllLogs().size();
    	Logs logs = logsService.findLogsByPersonAndProject(personService.findPersonsByLastName("Gormond").get(0), projectService.findProjectsByName("Kanban").get(0)).get(0);
    	
    	// Delete logs and check exception not throw
    	assertDoesNotThrow(() -> logsService.removeLogs(logs));
    	assertEquals(initialLogsListSize - 1, logsService.findAllLogs().size()); // Check logs list size
    	
    	// Check that exception is thrown
    	assertThrows(ObjectNotFoundException.class, () -> logsService.removeLogs(logs)); // Try to remove logs already delete
    }
}
