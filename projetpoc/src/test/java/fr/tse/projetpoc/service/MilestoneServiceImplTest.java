package fr.tse.projetpoc.service;


import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import fr.tse.projetpoc.dao.ProjectRepository;
import fr.tse.projetpoc.domain.Project;
import fr.tse.projetpoc.exception.BadArgumentException;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;

import fr.tse.projetpoc.domain.Milestone;
import fr.tse.projetpoc.domain.Person;
import javassist.tools.rmi.ObjectNotFoundException;

/**
 * Class used to test MilestoneServiceImplTest
 */
@SpringBootTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
@ActiveProfiles(profiles = "test")
public class MilestoneServiceImplTest {
	
	@Autowired
	private MilestoneService milestoneService;
	
	@Autowired
	private PersonService personService;
	
	@Autowired
	private LogsService logsService;

	@Autowired
	private ProjectRepository projectRepository;
	
	
	@Test
	@Order(1)
	public void findAllMilestonesTest() {
		List<Milestone> milestones = milestoneService.findAllMilestones();
		assertEquals(5, milestones.size());
	}
	
	@Test
	public void findMilestonesByProjectTest() {
		Project project = this.projectRepository.findAll().get(0);
		List<Milestone> expectedMilestones = project.getMilestones();
		List<Milestone> actualMilestones = this.milestoneService.findMilestonesByProject(project);
		assertEquals(expectedMilestones.size(), actualMilestones.size());
		assertEquals(expectedMilestones.get(0).getId(), actualMilestones.get(0).getId());
	}
	
	@Test
	public void findMilestonesByIdTest() {
		Milestone milestoneExpected = milestoneService.findAllMilestones().get(0);
		Milestone milestoneActual = milestoneService.findMilestoneById(milestoneExpected.getId());
		assertEquals(milestoneExpected.getId(), milestoneActual.getId());
		assertEquals(milestoneExpected.getDescription(), milestoneActual.getDescription());
	}
	
	@Test
	public void findMilestonesByDescriptionTest() {
		Milestone milestoneExpected = milestoneService.findAllMilestones().get(0);
		Milestone milestoneActual = milestoneService.findMilestonesByDescription(milestoneExpected.getDescription()).get(0);
		assertEquals(milestoneExpected.getId(), milestoneActual.getId());
	}
	
	@Test
	public void removeMilestoneTest() throws ObjectNotFoundException {
		Milestone milestone = milestoneService.findMilestonesByDescription("Realisation").get(0);
		int initialMilestonesListSize = milestoneService.findAllMilestones().size();
		
		// Delete milestone and check exception not throw
		assertDoesNotThrow(() -> milestoneService.removeMilestone(milestone));
		assertEquals(initialMilestonesListSize - 1, milestoneService.findAllMilestones().size());		
	}

	@Test
	public void findMilestoneFromProjectTest() throws BadArgumentException {
		Project projectExpected = this.projectRepository.findById(1L).orElse(null);
		Project projectActual= milestoneService.findProjectFromMilestone(1L);
		assertEquals(projectExpected.getId(),projectActual.getId());

	}

	@Test
	public void addLogsToMilestoneTest() {
		Person developer = personService.findPersonsByLastName("Drogo").get(0);
		Milestone milestone = milestoneService.findAllMilestones().get(0);
		
		int initialLogsListSize = logsService.findAllLogs().size();
		
		// Add logs and check exception not throw
		assertDoesNotThrow(() -> milestoneService.addLogsToMilestone(milestone, developer, 0.8, LocalDate.of(2021,1,31), "Test"));
		
		assertEquals(initialLogsListSize + 1, logsService.findAllLogs().size());		
	}
}
