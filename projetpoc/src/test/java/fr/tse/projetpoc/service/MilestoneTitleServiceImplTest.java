package fr.tse.projetpoc.service;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.List;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;

import fr.tse.projetpoc.domain.MilestoneTitle;
import fr.tse.projetpoc.utils.Constants;

/**
 * Class used to test MilestoneTitleServiceImplTest
 */
@SpringBootTest
@ActiveProfiles(profiles = "test")
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class MilestoneTitleServiceImplTest {
	
	@Autowired
	private MilestoneTitleService milestoneTitleService;
	
	@Test
	@Order(1)
	public void findAllMilestonesTitleTest() {
		List<MilestoneTitle> milestoneTitles = milestoneTitleService.findAllMilestonesTitle();
		assertEquals(4, milestoneTitles.size());
	}
	
	@Test
	@Order(2)
	public void findMilestonesTitleByIdTest() {
		MilestoneTitle milestoneTitle = milestoneTitleService.findMilestonesTitleById(Constants.MILESTONE_INITIALISATION_ID);
		assertEquals(Constants.MILESTONE_INITIALISATION_LABEL, milestoneTitle.getTitle());
	}
	
	@Test
	@Order(3)
	public void addMilestoneTitleTest() {
		int initMilestoneTitleListSize = milestoneTitleService.findAllMilestonesTitle().size();
		milestoneTitleService.addMilestoneTitle(new MilestoneTitle(1000L, "Test"));
		assertEquals(initMilestoneTitleListSize + 1 , milestoneTitleService.findAllMilestonesTitle().size());
	}

	@Test
	@Order(4)
	public void addMultipleTimeSameMilestoneTitleTest() {
		int initMilestoneTitleListSize = milestoneTitleService.findAllMilestonesTitle().size();
		milestoneTitleService.addMilestoneTitle(new MilestoneTitle(1000L, "Test"));
		assertThrows(DataIntegrityViolationException.class, () -> milestoneTitleService.addMilestoneTitle(new MilestoneTitle(1001L, "Test")));
	}
}
