package fr.tse.projetpoc.service;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;

import fr.tse.projetpoc.domain.Milestone;
import fr.tse.projetpoc.domain.Person;
import fr.tse.projetpoc.domain.Project;
import fr.tse.projetpoc.domain.Role;
import fr.tse.projetpoc.exception.NotManagerException;
import fr.tse.projetpoc.exception.UserLoginAlreadyExists;
import fr.tse.projetpoc.utils.Constants;
import javassist.tools.rmi.ObjectNotFoundException;

/**
 * Class used to test PersonServiceImplTest
 */
@SpringBootTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
@ActiveProfiles(profiles = "test")
public class PersonServiceImplTest {
	
	@Autowired
	private PersonService personService;
	
	@Autowired
	private RoleService roleService;
	
	@Autowired
	private MilestoneService milestoneService;
	
	@Autowired
	private LogsService logsService;
	
	@Test
	@Order(1)
	public void findAllPersonTest() {
		List<Person> personList = personService.findAllPerson();
		assertEquals(4, personList.size());
	}
	
	@Test
	@Order(2)
	public void findPersonByIdTest() {
		Person personExpected = personService.findAllPerson().get(0);
		Person personActual = personService.findPersonById(personExpected.getId());
		assertEquals(personExpected.getLastName(), personActual.getLastName());
		assertEquals(personExpected.getFirstName(), personActual.getFirstName());
	}
	
	@Test
	@Order(3)
	public void findPersonsByFirstNameTest() {
		List<Person> personExpectedList = personService.findPersonsByFirstName("Richard");
		assertEquals(1, personExpectedList.size());
		assertEquals("Drogo", personExpectedList.get(0).getLastName());
	}
	
	@Test
	@Order(4)
	public void findPersonsByLastNameTest() {
		List<Person> personExpectedList = personService.findPersonsByLastName("Merley");
		assertEquals(1, personExpectedList.size());
		assertEquals("Audric", personExpectedList.get(0).getFirstName());
	}
	
	@Test
    public void addPersonTest() throws UserLoginAlreadyExists {
		int initialPersonListSize = personService.findAllPerson().size();
		Person person = new Person(null, "Nom", "Prenom", "nom.prenom", "password", roleService.findRoleById(Constants.ROLE_DEVELOPER_ID));
		personService.addPerson(person);
		assertEquals(initialPersonListSize + 1, personService.findAllPerson().size());
		assertTrue(personService.findAllPerson().contains(person)); // Check if the person added is present
		
		Person person2 = new Person(null, "Nom", "Prenom", "acerSwift3", "password", roleService.findRoleById(Constants.ROLE_DEVELOPER_ID));
		assertThrows(UserLoginAlreadyExists.class, () -> personService.addPerson(person2));
    }
	
	@Test
	public void deletePersonTest() {
		Person person = personService.findAllPerson().get(2);
		int initialPersonListSize = personService.findAllPerson().size();

		// Delete person and check exception not throw
		assertDoesNotThrow(() -> personService.deletePerson(person.getId()));

		assertEquals(initialPersonListSize - 1, personService.findAllPerson().size());
		
		assertFalse(personService.findAllPerson().contains(person)); // Check if the person deleted is not present
	}
	
	@Test
	public void modifyRoleTest() {
		Person person = personService.findPersonsByLastName("Bertin").get(0);
		Role roleAdmin = roleService.findRoleById(Constants.ROLE_ADMIN_ID);
		
		// Change role and check exception not throw
		assertDoesNotThrow(() -> personService.modifyRole(person, roleAdmin));
		
		assertEquals(roleAdmin, personService.findPersonsByLastName("Bertin").get(0).getRole());
	}
	
	@Test
	@Order(6)
	public void addOrChangeManagerTest() {
		Person developer = personService.findPersonsByLastName("Drogo").get(0);
		Person manager = personService.findPersonsByLastName("Bertin").get(0);
		
		// Add manager and check exception not throw
		assertDoesNotThrow(() -> personService.addOrChangeManager(developer, manager));
		
		Person developerAfterProcessing = personService.findPersonsByLastName("Drogo").get(0);
		
		// Check if it is the good manager
		assertEquals(manager, developerAfterProcessing.getManager());
		
		// Check the exception if the manager hasn't manager role
		Person notAManager = personService.findPersonsByLastName("Gormond").get(0);
		assertThrows(NotManagerException.class, () -> personService.addOrChangeManager(developer, notAManager));
		
		// Check the exception if the manager doesn't exist in database
		Person unknownManager = null;
		assertThrows(ObjectNotFoundException.class, () -> personService.addOrChangeManager(developer, unknownManager));

		// Check the exception if the developer doesn't exist in database
		Person unknownDeveloper = null;
		assertThrows(ObjectNotFoundException.class, () -> personService.addOrChangeManager(unknownDeveloper, manager));
	}
	
	
	@Test
	@Order(8)
	public void deleteManagerTest() {
		Person developer = personService.findPersonsByLastName("Drogo").get(0);
		
		// Delete manager and check exception not throw
		assertDoesNotThrow(() -> personService.deleteManager(developer));
		
		// Check the exception if the developer doesn't exist in database
		Person unknownDeveloper = null;
		assertThrows(ObjectNotFoundException.class, () -> personService.deleteManager(unknownDeveloper));
	}
	
	@Test
	public void addLogsToMilestoneTest() {
		Person developer = personService.findPersonsByLastName("Drogo").get(0);
		Milestone milestone = milestoneService.findAllMilestones().get(0);
		
		int initialLogsListSize = logsService.findAllLogs().size();
		
		// Add logs and check exception not throw
		assertDoesNotThrow(() -> personService.addLogsToUser(developer, milestone, 0.8, LocalDate.of(2021,1,31), "Test"));
		
		assertEquals(initialLogsListSize + 1, logsService.findAllLogs().size());		
	}
	
	@Test
	@Order(7)
	public void findAllDeveloperForManagerTest() throws ObjectNotFoundException, NotManagerException {
		Person manager = personService.findPersonsByLastName("Bertin").get(0);
		
		// Retrieve all developers of the manager
		List<Person> personList = personService.findAllDeveloperForManager(manager);
		
		assertEquals(2, personList.size());
		Person developerMerley = personService.findPersonsByLastName("Merley").get(0);
		Person developerDrogo = personService.findPersonsByLastName("Drogo").get(0);
		assertTrue(personList.contains(developerDrogo));
		assertTrue(personList.contains(developerMerley));
		
		// Check the exception if the manager has no manager role
		Person notAManager = personService.findPersonsByLastName("Drogo").get(0);
		assertThrows(NotManagerException.class, () -> personService.findAllDeveloperForManager(notAManager));
	}
	
	
	@Test
	@Order(5)
	public void findAllProjectForPersonTest() throws ObjectNotFoundException {
		Person developer = personService.findPersonsByLastName("Gormond").get(0);
		
		// Retrieve all projects of the person
		List<Project> projects = personService.findAllProjectForPerson(developer);
		
		assertEquals(2, projects.size());
	}
}
