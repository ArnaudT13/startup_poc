package fr.tse.projetpoc.service;


import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Profile;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;

import fr.tse.projetpoc.dao.ProjectRepository;
import fr.tse.projetpoc.domain.Milestone;
import fr.tse.projetpoc.domain.Person;
import fr.tse.projetpoc.domain.Project;
import fr.tse.projetpoc.exception.BadArgumentException;
import javassist.tools.rmi.ObjectNotFoundException;

@SpringBootTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
@ActiveProfiles(profiles = "test")
public class ProjectServiceImplTest {
	
	@Autowired
	private ProjectService projectService;
	
	@Autowired
	private ProjectRepository projectRepository;
	
	@Autowired
	private PersonService personService;
	
	@Autowired
	private MilestoneService milestoneService;
	

	@Test
	@Order(1)
	public void findAllProjectTest() {
		List<Project> projects = projectService.findAllProject();
		assertEquals(3, projects.size());
	}
	
	@Test
	public void findProjectByIdTest() {
		Project projectExpected = projectService.findAllProject().get(0);
		Project projectActual  = projectService.findProjectById(projectExpected.getId());
		assertEquals(projectExpected.getName(), projectActual.getName());
	}
	
	@Test
	public void findProjectsByNameTest() {
		List<Project> projects = projectService.findProjectsByName("Kanban");
		assertEquals(1, projects.size());
		assertEquals("Kanban", projects.get(0).getName());
	}
	
	@Test
	public void addProjectTest() {
		int initialProjectListSize = projectService.findAllProject().size();
		Project project = projectService.addProject(new Project(null, "Test"));
		assertEquals(initialProjectListSize + 1, projectService.findAllProject().size());
		projectRepository.delete(project);
	}
	
	// TODO Pass test
	@Test
	public void deleteProjectTest() {
		// Search elements in database for testing
		int initialProjectListSize = projectService.findAllProject().size();
		Project project = projectService.findProjectsByName("POC").get(0);
		
		// Delete project
		assertDoesNotThrow(() -> projectService.deleteProject(project));
		
		assertEquals(initialProjectListSize - 1, projectService.findAllProject().size());
		
		assertThrows(ObjectNotFoundException.class, () ->  projectService.deleteProject(new Project(20L, "Test"))); // Try to delete unknown project
	}
	
	@Test
	public void addMilestoneInProjectTest() throws BadArgumentException {
		// Search elements in database for testing
		Project project = projectService.findProjectsByName("Kanban").get(0);
		Milestone milestone = milestoneService.findMilestonesByDescription("Deployment").get(0);
		
		// Get current size
		int initialMilestoneListSize = project.getMilestones().size();
		
		// Add milestone
		project = projectService.addMilestoneInProject(project.getId(), milestone);
		
		assertEquals(initialMilestoneListSize + 1, project.getMilestones().size()); // Check milestones list size
		assertTrue(project.getMilestones().contains(milestone)); // Check if the milestone added is present
		
		assertThrows(BadArgumentException.class, () ->  projectService.addMilestoneInProject(99999L, milestone)); // Try to add milestone with a wrong project id
	}
	
	@Test
	public void deleteMilestoneInProjectTest() throws BadArgumentException, ObjectNotFoundException {
		// Search elements in database for testing
		Project project = projectService.findProjectsByName("Kanban").get(0);
		Milestone milestone = milestoneService.findMilestonesByDescription("Persistence Conception").get(0);
		
		// Get current size
		int initialMilestoneListSize = project.getMilestones().size();

		// Delete milestone
		project = projectService.deleteMilestoneInProject(project.getId(), milestone);
		
		assertEquals(initialMilestoneListSize - 1, project.getMilestones().size()); // Check milestones list size
		assertFalse(project.getMilestones().contains(milestone)); // Check if the milestone deleted is not present
		
		assertThrows(BadArgumentException.class, () ->  projectService.deleteMilestoneInProject(99999L, milestone)); // Try to delete milestone with a wrong project id
	}
	
	@Test
	public void addContributorInProjectTest() throws BadArgumentException {
		// Search elements in database for testing
		Project project = projectService.findProjectsByName("Kanban").get(0);
		Person person = personService.findPersonsByLastName("Bertin").get(0);
		
		// Get current size
		int initialContributorListSize = project.getContributors().size();
		
		// Add contributor
		project = projectService.addContributorInProject(project.getId(), person);
		assertEquals(initialContributorListSize + 1, project.getContributors().size()); // Check contributors list size
		assertTrue(project.getContributors().contains(person)); // Check if the contributor added is present
		
		assertThrows(BadArgumentException.class, () ->  projectService.addContributorInProject(99999L, person)); // Try to add contributor with a wrong project id
	}
	
	@Test
	public void deleteContributorInProjectTest() throws BadArgumentException {
		// Search elements in database for testing
		Project project = projectService.findProjectsByName("Kanban").get(0);
		Person person = personService.findPersonsByLastName("Gormond").get(0);
		
		// Get current size
		int initialContributorListSize = project.getContributors().size();
		
		// Delete contributor
		project = projectService.deleteContributorInProject(project.getId(), person);
		assertEquals(initialContributorListSize - 1, project.getContributors().size()); // Check contributors list size
		assertFalse(project.getContributors().contains(person)); // Check if the contributor deleted is not present
		
		assertThrows(BadArgumentException.class, () ->  projectService.deleteContributorInProject(99999L, person)); // Try to delete contributor with a wrong project id
	}
	
}
