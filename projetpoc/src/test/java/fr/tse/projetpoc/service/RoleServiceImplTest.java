package fr.tse.projetpoc.service;


import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import fr.tse.projetpoc.utils.RoleType;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;

import fr.tse.projetpoc.domain.Role;
import fr.tse.projetpoc.utils.Constants;

@SpringBootTest
@TestMethodOrder(MethodOrderer.Alphanumeric.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
@ActiveProfiles(profiles = "test")
public class RoleServiceImplTest {
	
	@Autowired
	private RoleService roleService;
	
	@Test
	public void findAllRoleTest() {
		List<Role> roles = roleService.findAllRole();
		assertEquals(3, roles.size());
	}
	
	@Test
	public void findRoleByIdTest() {
		Role role = roleService.findRoleById(Constants.ROLE_ADMIN_ID);
		assertEquals(RoleType.Admin, role.getLabel());
	}
}
